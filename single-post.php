<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NMC_Theme
 */

get_header(); ?>
<?php view('general.cover'); ?>
<div class="single-post">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php while( have_posts() ) : the_post(); ?>
                    <div class="single-post__title">
                        <h4 class="mb-0"><?php the_title(); ?></h4>  
                    </div>
                    <div class="single-post__navigation">
                        <?php woocommerce_breadcrumb(); ?>
                    </div>
                    <div class="single-post__content">
                        <?php the_content(); ?>
                    </div>
                    <div class="single-post__social-share">
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_button_facebook"><?php pll_e('Facebook'); ?></a> 
                            <a class="a2a_button_twitter"><?php pll_e('Twitter'); ?></a> 
                            <a class="a2a_button_linkedin"><?php pll_e('LinkedIn'); ?></a> 
                            <!-- <a class="a2a_dd" href="https://www.addtoany.com/share"></a> -->
                        </div>
                        <script async src="https://static.addtoany.com/menu/page.js"></script>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>
<div class="latest-articles">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="latest-articles__header">
                    <h2><?php pll_e('Latest Articles'); ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php view('general.latest-articles'); ?>
        </div>
    </div>
</div>
<?php get_footer();
