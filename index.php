<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NMC_Theme
 */

get_header(); ?>
 
    <?php view('home.menu-slider'); ?>
    <?php view('home.services'); ?>
    <?php //view('home.featured-products'); ?>
    <?php view('home.featured-products-slider'); ?>
    <?php view('home.banner'); ?>
    <div class="section-shape">
        <?php view('home.new-products'); ?>
        <?php view('home.blog'); ?>
    </div>
    <?php //view('general.location'); ?>
<?php get_footer();
