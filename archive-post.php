<?php get_header(); ?>
    <div class="container py-5">
        <div class="row">
            <?php while( have_posts() ) : the_post(); ?>
                <div class="col-12 col-lg-6">
                    <?php view('general.article-card'); ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php get_footer(); ?>