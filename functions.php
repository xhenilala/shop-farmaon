<?php
/**
 * NMC Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NMC_Theme
 */

require get_template_directory() . '/vendor/autoload.php';


App\Init::init();



function my_custom_upload_mimes($mimes = array()) {

	$mimes['csv'] = "text/csv";
	return $mimes;
}
add_action('upload_mimes', 'my_custom_upload_mimes');


add_action('init',function(){
	if(isset($_GET['delete'])){
		$term_id = 620;
		$parent = get_term_by( 'id',$term_id, 'product_cat');
		
		$terms =  get_term_children( $term_id, 'product_cat' );
		foreach($terms as $term_id){
			wp_delete_term( $term_id, 'product_cat' );
		}

	}
});

if( isset($_GET['export_products'])){
	add_action( 'init', function(){
		global $wpdb;
		$results = $wpdb->get_results( "SELECT ID FROM wp_posts WHERE post_type = 'product' LIMIT 200000" );
		if( $results ){ ?>
			<table>
			<tr>
				<td>SKU</td>
				<td>Name</td>
				<td>Short Description</td>
			</tr>
			<?php
			foreach( $results as $row ){
				$post_id = $row->ID;
				$_product = wc_get_product( $post_id );
				if( 'simple' == $_product->get_type() ){
					echo sprintf( '<tr>
						<td>%s</td>
						<td>%s</td>
					</tr>', $_product->get_sku(), $_product->get_name() );

				} elseif( 'variable' == $_product->get_type() ){
					$variation_ids = $_product->get_children();
					if( $variation_ids ){
						foreach( $variation_ids as $id ){
							$_variation = wc_get_product( $id );
							echo sprintf( '<tr>
								<td>%s</td>
								<td>%s</td>
							</tr>', $_variation->get_sku(), $_variation->get_name() );
							
						}	
					}
				}	
			}?>
			</table>
			<?php
			die;
		} 
	} );
	
}
//  add_action('init',function(){

//     $locale = get_locale();
//     var_dump($locale);
//     $current_language = pll_current_language();
//     //  dd($current_language);
//     $cur_lang = get_bloginfo("language");
//     var_dump($cur_lang);
//     // var_dump($cur_lang, $current_language,pll_the_languages());
//  },100);