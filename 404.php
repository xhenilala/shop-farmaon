<?php get_header(); ?>
<?php view('general.default-cover'); ?>
    <div class="container py-5">
        <div class="row py-5">
            <div class="col text-center">
                <h1 class="m-0">
                    <?php pll_e('Page Not Found'); ?> <br>
                    <a href="<?php echo home_url(); ?>" class="mt-4 btn btn-primary"><?php pll_e('Return to Homepage'); ?></a>
                </h1>
            </div>
        </div>
    </div>
<?php get_footer(); ?>