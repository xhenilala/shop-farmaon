<?php
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( !ini_get( "auto_detect_line_endings" ) ) {
    ini_set( "auto_detect_line_endings" , '1' );
}

if ( ! class_exists( 'WooCommerce_Csv_Import' ) ) :

/**
 * Main WooCommerce csv import Class.
 * @version	1.0.0
 */
class WooCommerce_Csv_Import{
	
    /**
     * Csv delimiter
     * @var string
     */
    public static $csv_delimiter = ';';

    /**
     * Images base path
     * @var string
     */
    public $images_base_path;

    /**
     * Allow product import
     */
    const ALLOW_IMPORT = true;

    /**
     * Product column numbers
     */
    const PRODUCT_CODE_COL          = 0;
    const PRODUCT_SKU_COL           = 1;
    const PRODUCT_SIZE_COL          = 2;
    const PRODUCT_COLOR_COL         = 3;
    const PRODUCT_NAME_COL          = 4;
    const PRODUCT_CLASS_COL         = 5;
    const PRODUCT_CATGORY_COL       = 6;
    const PRODUCT_BRAND_COL         = 7;
    const PRODUCT_STOCK_COL         = 8;
    const PRODUCT_PRICE_COL         = 9;
    const PRODUCT_SALE_PRICE_COL    = 10;
    const PRODUCT_DISCOUNT_COL      = 11;
    const PRODUCT_DESCRIPTION_COL   = 12;
    const PRODUCT_OUTLET_COL        = 13;
    const PRODUCT_PROMO_COL         = 14;
    const PRODUCT_PROMO_TWO         = 15;

    /**
     * Author id for new post 
     */
    const PRODUCT_AUTHOR_ID = 1;

	function __construct(){

		$this->log = new Logger( 'import_log' );
		$this->log_path = ABSPATH . 'logs/';
		$this->log->pushHandler( new StreamHandler( $this->log_path . sprintf( 'import_%s.log', date('Y_m_d') ) ) );

        $this->product_images_base_path = ABSPATH . '_product_images/';

		do_action( 'woocommerce_csv_import_loaded' );

        if ( is_admin() ) {
            //add_action( 'admin_menu', array( $this, 'import_products_menu' ) );
            add_action( 'woocommerce_csv_import_on_form_submit' , array( $this, 'import_products' ) );
        }

        add_filter( 'product_color', array( $this, 'product_color_replace' ) );
        add_filter( 'product_class', array( $this, 'product_class_replace' ) );
        add_filter( 'product_brands', array( $this, 'product_brands_replace' ) );
        add_filter( 'product_gender', array( $this, 'product_gender_replace' ) );

        add_filter( 'product_price', array( $this, 'product_price' ) );
        
	}

    public function product_price( $price ){
        return trim( str_replace( ',', '', $price ) );
    }

    public function get_term_id_by_term_root( $term_name, $root_term ){
        
        global $wpdb;

        if ( !$term_name || !$root_term )
            return;

        $root_term = strtolower( $root_term );

        $terms = $wpdb->get_results( sprintf( 
            'select * from %s terms left join %s term_rel on terms.term_id = term_rel.term_taxonomy_id where terms.name = "%s"',
            $wpdb->terms, $wpdb->term_relationships , $term_name
        ), ARRAY_A );

        if ( count( $terms )  > 0 ){
            foreach ( $terms as $term ){
                if ( preg_match('|'.$root_term.'|ims', $term['slug'] ) ){
                    return (int) $term['term_id'];
                }
            }
        }

        return false;
    }

    public function prepare_product_terms( $term_name, $taxonomy_name, $root_category = false ){

        if ( $taxonomy_name == 'product_cat' && $root_category ) {
            $term_id = $this->get_term_id_by_term_root( $term_name, $root_category );
        } else {
            $term_object = get_term_by( 'name' , $term_name , $taxonomy_name, ARRAY_A );
            if ( !is_array( $term_object ) || empty( $term_object ) ){
                return false;
            }
            $term_id = $term_object['term_id'];
        }

        $terms = array();

        if ( is_int( $term_id ) && $term_id > 0 ){
            $terms = get_ancestors( $term_id, $taxonomy_name );
        }

        $terms[] = $term_id;
        return $terms;
    }

    /**
     * Get product images
     * @param  string $product_code
     * @return 
     */
    public function get_product_images( $product_code ){

        $product_code = str_replace(' ', '', $product_code);

        if ( !$product_code ){
            return new WP_Error( 'error', __( "Missing product code", "woocommerce_csv_import" ) );
        }

        if ( !file_exists( $this->product_images_base_path . $product_code ) ){
            
            $this->log->warning( sprintf( 
                "Directory %s does not exist", 
                $this->product_images_base_path . $product_code ) 
            );

            return false;
        }

        $files = array_diff( scandir(  $this->product_images_base_path . $product_code ), array( '..' , '.' ) );
        if ( is_array( $files ) && !empty( $files ) ){
            
            $product_images = array();
            foreach ( $files as $file ){
                if ( $file[0] !== '.')
                    $product_images[] = $this->product_images_base_path . $product_code . '/' . $file;
            }
            return $product_images;

        } else {
            
            $this->log->warning( sprintf( 
            	"No files found for:  %s", $product_code ) 
            );

            return false;
        }
    }

    // public function product_gender_replace( $product_class ){
        
    //     if ( preg_match( '#(vajz)#ims', $product_class ) ) {
    //         $product_class = 'Vajza';
    //     }

    //     if ( preg_match( '#(djem)#ims', $product_class ) ) {
    //         $product_class = 'Djem';
    //     }

    //     $terms = $this->prepare_product_terms( $product_class, 'product_gender' );
    //     return $terms;
    // }

    /**
     * Create product gallery
     * @param  int $post_id
     * @param  array $images
     * @return void
     */
    public function create_product_gallery( $post_id, $images ){
        
        if ( !$post_id ) {
            return new WP_Error( 'error', __( "Missing post id, or post does not exist.", "woocommerce_csv_import" ) );
        }

        if( !is_array( $images ) || empty( $images ) ){
        	$this->log->warning( sprintf( 
            	"Cannot create gallery for:  %s no images found", $post_id ) 
            );
            return new WP_Error( 'error', __( "No images provided.", "woocommerce_csv_import" ) );
        }

        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        $image_ids = array();
        foreach( $images as $image ){
            $image_ids[] = $this->_upload_file( $image );
        }

        if ( !empty( $image_ids ) ) {
            // Set woocommerce gallery
            update_post_meta( $post_id , '_product_image_gallery', implode( ',' , $image_ids ) );
            // Set wordpress featured image
            set_post_thumbnail( $post_id, $image_ids[0] );
        }
    }

    /**
     * Upload file to wordpress
     * @param  string $file
     * @return int - attachment id on success, false otherwise
     */
    public function _upload_file( $file ){
        
        $filename = basename( $file );
        $upload_file = wp_upload_bits( $filename, null, file_get_contents( $file ) );

        if ( !$upload_file['error'] ) {
            $parent_post_id = 0;
            $wp_filetype = wp_check_filetype( $filename, null );
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_parent' => $parent_post_id,
                'post_title' => preg_replace( '/\.[^.]+$/', '', $filename ),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $parent_post_id );            
            if ( !is_wp_error( $attachment_id ) ) {
                $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
                wp_update_attachment_metadata( $attachment_id,  $attachment_data );
                return $attachment_id;
            }
        }

        return false;
    }



    

    // /**
    //  * Upload Csv file
    //  * @return int - wp attachment id on success, false otherwise
    //  */
    // public function upload_file(){
        
    // 	// Security check.
    // 	if ( isset( $_POST['woocommerce_csv_import_nonce'] ) 
    // 		&& wp_verify_nonce( $_POST['woocommerce_csv_import_nonce'], 'upload_csv_file' ) ) {

	// 		require_once( ABSPATH . 'wp-admin/includes/image.php' );
	// 		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	// 		require_once( ABSPATH . 'wp-admin/includes/media.php' );

	// 		// Upload file, get attachment id.
	// 		$attachment_id = media_handle_upload( 'upload_csv_file', null );
			
	// 		if ( !is_wp_error( $attachment_id ) )
	// 			return $attachment_id;
	// 	}

	// 	return false;
    // }

    /**
     * Set product color
     * @param  string $color
     * @return string
     */
    public function product_color_replace( $color ){

        $find = array( 
            '217-LT GREY MEL/0',
            '301-NAVY/0',
            '100-WHITE/0',
            '8816-OLD WINE/0',
            '3012-INDIGO MEL/0',
            '627-RED MEL/0',
            '1332-ELFIN YELLOW/0'
        );

        $replace = array(
            'Grey Mel',
            'Navy',
            'White',
            'Wine',
            'Indigo Mel',
            'Red Mel',
            'Elfin Yellow'
        );

        $color = str_ireplace( $find, $replace, $color );
        return $color;
    }

    /**
     * Set product class
     * @param  string $class
     * @return string
     */
    public function product_class_replace( $class ){
        
        $find = array(
            'MAN',
            'WOMAN',
            'KIDS',
        );
        
        $replace = array(
            'Meshkuj',
            'Femra',
            'Femije',
        );

        $class = str_ireplace( $find, $replace, $class );
        return $class;
    }

    /**
     * Set product categories
     * @param  string $product_categories
     * @return string
     */
    public function product_categories_replace( $product_categories , $product_class ){

        if ( !$product_categories )
            return;

        if ( preg_match( '#unisex#ims', $product_class ) ) {
            $product_class = array( 'Meshkuj', 'Femra' );
        }

        elseif ( preg_match( '#(vajza|djem)#ims', $product_class ) ) {
            $product_class = 'Femije';
        }

        $find = array( 'find' );
        $replace = array( 'replace' );

        if ( preg_match('|\,|', $product_categories ) )
            $product_categories = explode( ',', $product_categories );

        if ( is_array( $product_categories )  && !empty( $product_categories ) ) {            
            
            foreach( $product_categories as $k => $category ){
                $product_categories[$k] = str_ireplace( $find, $replace, $category );
            }

            $cats =  $this->get_product_terms(  $product_categories , $product_class );
        }

       $cats = $this->get_product_terms(  array( str_ireplace( $find, $replace, $product_categories ) ) , $product_class );

       return $cats;
    }

    public function get_product_terms( $product_categories , $product_class ){

        if ( is_array( $product_categories ) && !empty( $product_categories ) && $product_class ) {
            
            foreach ( $product_categories as $k => $category ) {
               
                if ( is_array( $product_class ) && !empty( $product_class ) ) {
                    
                    foreach ($product_class as $class) {
                        $product_terms[] = $this->prepare_product_terms( $category, 'product_cat', $class );
                    }
                    
                    if ( is_array( $product_terms[0] ) && is_array( $product_terms[1] ) ) 
                        $product_terms = array_merge( $product_terms[0], $product_terms[1] );

                } else {
                    $product_terms[$k] = $this->prepare_product_terms( $category, 'product_cat', $product_class );
                }
            }

            return $product_terms;
        }

        return $product_categories;
    }
    
    // /**
    //  * Set product brands
    //  * @param  string $brand
    //  * @return string
    //  */
    // public function product_brands_replace( $brand ){

    //     $find = array(
    //         'LTB'
    //     );

    //     $replace = array(
    //         'LTB Jeans'
    //     );

    //     if ( preg_match('|\,|', $brand ) )
    //         $product_brands = explode( ',', $brand );

    //     if ( is_array( $product_brands )  && !empty( $product_brands ) ) {            
            
    //         foreach( $product_brands as $k => $brand_name ){
    //             $product_brands[$k] = str_ireplace( $find, $replace, $brand_name );
    //         }
    //         return $product_brands;
    //     }

    //     return array( str_ireplace( $find, $replace, $brand ) );
    // }

   
    /**
     * Extract product details
     * @param  array $row ( csv row )
     * @return array
     */
    public function extract_product_row( $row ){
        
        $outlet = ( $row[self::PRODUCT_OUTLET_COL] == 1 ) 
        ? $this->product_categories_replace( 'Outlet', $row[self::PRODUCT_CLASS_COL] ) : false;
        
        $product = array(
            'code'        => apply_filters( 'product_code' ,        $row[self::PRODUCT_CODE_COL] ),
            'sku'         => apply_filters( 'product_sku' ,         $row[self::PRODUCT_SKU_COL] ),
            'size'        => apply_filters( 'product_size' ,        $row[self::PRODUCT_SIZE_COL] ),
            'color'       => apply_filters( 'product_color' ,       $row[self::PRODUCT_COLOR_COL] ),
            'name'        => apply_filters( 'product_name' ,        $row[self::PRODUCT_NAME_COL] ),
            'class'       => apply_filters( 'product_class' ,       $row[self::PRODUCT_CLASS_COL] ),
            'categories'  => $this->product_categories_replace( $row[self::PRODUCT_CATGORY_COL], $row[self::PRODUCT_CLASS_COL] ),
            'brands'      => $this->prepare_product_terms( $row[self::PRODUCT_BRAND_COL], 'brand' ),
            'stock'       => apply_filters( 'product_stock' ,       $row[self::PRODUCT_STOCK_COL] ),
            'price'       => apply_filters( 'product_price' ,       $row[self::PRODUCT_PRICE_COL] ),
            'sale_price'  => apply_filters( 'product_price' ,  $row[self::PRODUCT_SALE_PRICE_COL] , $row[self::PRODUCT_PRICE_COL]),
            'description' => apply_filters( 'product_description' , $row[self::PRODUCT_DESCRIPTION_COL] ),
            'outlet'      => $outlet,
            'discount'    => apply_filters( 'product_discount' ,    $row[self::PRODUCT_DISCOUNT_COL] ),
            'gender'      => apply_filters( 'product_gender' ,      $row[self::PRODUCT_CLASS_COL] ),
            'promo'       => apply_filters( 'product_promo' ,      $row[self::PRODUCT_PROMO_COL] ),
            'promo_two'       => apply_filters( 'product_promo' ,      $row[self::PRODUCT_PROMO_TWO] ),
        );

        array_walk( $product, array( $this, 'trim_values' ) );
        return $product;
    }

    public function trim_values( $val ){

        if ( is_string( $val ) )
            return trim( $val );

        return $val;
    }

    /**
     * Import porducts
     * @return void
     */
    public function import_products( $file_name = false ){


        if ( !isset( $_POST['upload_csv'] ) && !$file_name )
    		return;

        if ( !$file_name ) {
        	$attachment_id = self::upload_file();
        	if ( !$attachment_id ){
        		echo __( 'Missing attachment id', 'woocommerce_csv_import' );
        		return;
        	}

        	$file_name = get_attached_file( $attachment_id );
        }

    	$product_rows = $this->read_csv( $file_name );

    	if ( !$product_rows ){
    		echo __( 'This is not a valid csv file', 'woocommerce_csv_import' );
    		return;
    	}

    	foreach( $product_rows as $row_num => $product_row ){
            
            // exclude header
            if ( $row_num > 0 ){
                
                // extract fields
                $product = $this->extract_product_row( $product_row );

                // check if product code exist
                if ( $product['code'] ) {
                    $products[ $product['code'] ][ 'data' ] = $product;
                    $products[ $product['code'] ][ 'variations' ][ $product['sku'] ] = $product;
                }
            }
        }  

        $porducts_formated = array();

        $i = 0; 
        foreach ( $products as $key => $product ) {

            if ( is_array( $product['variations'] ) ) {

                $j = 0; 
                foreach ( $product['variations'] as $e => $product_variation ) {
                    $variations[$i][$j] = array(
                        'attributes' => array(
                            'madhesia'  => $product_variation['size'],
                            'ngjyra'    => $product_variation['color'],
                        ),
                        'price'         =>  $product_variation['price'],
                        'sale_price'    =>  $product_variation['sale_price'],
                        'sku'           =>  $product_variation['sku'],
                        'manage_stock'  => 'yes',
                        'stock'         => $product_variation['stock'],
                        'backorders'    => 'no',
                        'discount'      => $product_variation['discount']
                    ); 
                $j++;
                }
            }

            $porducts_formated[$i] = array(
                'code'          => $product['data']['code'],
                'name'          => $product['data']['name'],
                'sku'           => $product['data']['sku'],
                'price'         => $product['data']['price'],
                'sale_price'    => $product['data']['sale_price'],
                'description'   => $product['data']['description'],
                'categories'    => $product['data']['categories'],
                'brands'        => $product['data']['brands'],
                'outlet'        => $product['data']['outlet'],
                'gender'        => $product['data']['gender'],
                'promo'         => $product['data']['promo'],
                'promo_two'     => $product['data']['promo_two'],
                'available_attributes'  => array( 'madhesia', 'ngjyra' ),
                'variations'            => $variations[$i]
            );

            $product_images = $this->get_product_images( $porducts_formated[$i]['code'] );
            
            if ( self::ALLOW_IMPORT && !self::product_exist( $porducts_formated[$i]['code'] ) && $product_images != false ) {
                $this->insert_product( $porducts_formated[$i] );
                $this->log->warning( sprintf( 
            		"Inserting product :  %s", $porducts_formated[$i]['code']) 
           		);
            }

            if ( self::ALLOW_IMPORT && self::product_exist( $porducts_formated[$i]['code'] )) {
                $this->update_product( $porducts_formated[$i] );
                $this->log->warning( sprintf( 
            		"Updating product :  %s", $porducts_formated[$i]['code']) 
           		);
            }

        $i++;
        }
    }

    /**
     * Update product stock, price, sale price
     * @param  array $product_data
     * @return void
     */
    public function update_product( $product_data ){

        $post_id = wc_get_product_id_by_sku( $product_data['code'] );
        $product_variations = $product_data['variations'];
        
        if ( is_array( $product_variations ) && !empty( $product_variations ) ) {

            foreach ( $product_variations as $variation ) {
                $variation_post_id = wc_get_product_id_by_sku( $variation['sku'] );
                
                if ( $variation_post_id ) {
                    $this->log->warning( sprintf( "Updating variation:  %s", $variation['sku'] ) );

                    if ( $variation['price'] ){
                        update_post_meta( $variation_post_id, '_regular_price', $variation['price'] );
                    }

                    if ( $variation['sale_price'] && ($variation['sale_price'] != $variation['price'])){
                        update_post_meta( $variation_post_id, '_sale_price', $variation['sale_price'] );
                        update_post_meta( $variation_post_id, '_price', $variation['sale_price'] );
                    } 
                    else{
                        update_post_meta( $variation_post_id, '_price', $variation['price'] );
                    }

                    if($variation['sale_price'] == $variation['price'])
                        delete_post_meta( $variation_post_id, '_sale_price');

                    //update_post_meta( $variation_post_id, '_stock', $variation['stock'] );
                }

                else{
                    $this->insert_product_attributes( $post_id, $product_data['available_attributes'], $product_data['variations'] );
                    $this->insert_product_variations( $post_id, $product_data['variations'] );
                }
            }
        }

        
        WC_Product_Variable::sync_stock_status( $post_id );
        WC_Product_Variable::sync( $post_id );

        wc_delete_product_transients( $post_id );
        
    }

    /**
     * Update product ( !! )
     * @return void
     */
    public function _update_product( $product_data ){
        
        $product_id     = wc_get_product_id_by_sku( $product_data["code"] );
        $product        = wc_get_product($product_id);
        
        if($product->is_type( 'variable' ))
            $variations     = $product->get_available_variations();
        
        #50% discount on LTB, Polaris and Kinetix products
        $brandsobj  = get_the_terms( $product_id, 'brand' );
        $brandnames = array();

        if ( $brandsobj && ! is_wp_error( $brandsobj ) ) :
            foreach ( $brandsobj as $term )
                $brandnames[] = $term->name;
        endif;

        if(array_uintersect($brandnames, array('LTB', 'Polaris', 'Kinetix'), "strcasecmp") && !empty($variations) && !$product->is_on_sale()){

            foreach ($variations as $variation) {
                $variation_post_id = $variation["variation_id"];

                $product_variation = new WC_Product_Variation($variation_post_id);
                
                update_post_meta( $variation_post_id, "_sale_price", 0.5*$product_variation->get_regular_price());
                update_post_meta( $variation_post_id, "_price", 0.5*$product_variation->get_regular_price());

                $this->log->warning( sprintf( "Product has term %s. product_id: %s</strong>",$brandnames[count($brandnames)-1], $product_id));

                clean_post_cache( $variation_post_id );
            }

            update_post_meta(  $product_id, "_price", 0.5*$product->get_price());
            //update_post_meta(  $product_id, "_min_variation_price", 0.5*$product->get_price());
            //update_post_meta(  $product_id, "_min_variation_sale_price", 0.5*$product->get_price());

          //  wc_delete_product_transients( $product_id);
        }
    }

    /**
     * Insert product
     * @param  array $product_data 
     * @return void
     */
    public function insert_product ( $product_data ){
        
        $post = array(
            'post_author'  => self::PRODUCT_AUTHOR_ID,
            'post_content' => $product_data['description'],
            'post_status'  => 'publish',
            'post_title'   => $product_data['name'],
            'post_parent'  => '',
            'post_type'    => 'product'
        );

        $post_id = wp_insert_post( $post );

        if ( !$post_id )
            return false;

        $this->log->warning( sprintf( 
    		"Product :  %s inserted successfully with post_id: %d</strong>", $product_data['code'], $post_id) 
   		);
        update_post_meta( $post_id , '_sku', $product_data['code'] );
        update_post_meta( $post_id , '_visibility' , 'visible' );

        wp_set_object_terms( $post_id, 'variable', 'product_type' );
        
        if ( is_array($product_data['categories']) && !empty( $product_data['categories'] ) ) {
            foreach($product_data['categories'] as $categories){
                wp_set_object_terms( $post_id, $categories, 'product_cat', true );
            }
        }

        if ( is_array($product_data['brands']) && !empty( $product_data['brands'] ) ) {
            foreach($product_data['brands'] as $brand ){
                wp_set_object_terms( $post_id, $brand, 'brand' , true );
            }
        }

        if ( is_array($product_data['outlet']) && !empty( $product_data['outlet'] ) ) {
            foreach($product_data['outlet'] as $outlet){
                wp_set_object_terms( $post_id, $outlet, 'product_cat', true );
            }
        }

        if ( is_array($product_data['gender']) && !empty( $product_data['gender'] ) ) {
            foreach($product_data['gender'] as $gender){
                wp_set_object_terms( $post_id, $gender, 'product_gender', true );
            }
        }

        if ( $product_data['promo'] ) {
            
            $product_tag_term = wp_insert_term(  trim( $product_data['promo'] ), 'product_tag' );
            wp_set_object_terms( $post_id , array( trim( $product_data['promo'] ) ), 'product_tag', true );

            $this->log->warning( sprintf( "Creating product tag: %s", $product_data['promo'] ) );
        }

         if ( $product_data['promo_two'] ) {
            
            $product_tag_term = wp_insert_term(  trim( $product_data['promo_two'] ), 'product_tag' );
            wp_set_object_terms( $post_id , array( trim( $product_data['promo_two'] ) ), 'product_tag', true );

            $this->log->warning( sprintf( "Creating product tag: %s", $product_data['promo_two'] ) );
        }
        
        $this->insert_product_attributes( $post_id, $product_data['available_attributes'], $product_data['variations'] );
        $this->insert_product_variations( $post_id, $product_data['variations'] );

        $product_images = $this->get_product_images( $product_data['code'] );
        if ( is_array( $product_images ) ){
            $this->create_product_gallery( $post_id, $product_images );
        }

        
        WC_Product_Variable::sync_stock_status( $post_id );
        WC_Product_Variable::sync( $post_id );


        wc_delete_product_transients( $post_id );
    }

    /**
     * Insert product attributes
     * @param  int $post_id
     * @param  array $available_attributes
     * @param  array $variations
     * @return void
     */
    public function insert_product_attributes ( $post_id, $available_attributes, $variations ){
        
        foreach ( $available_attributes as $attribute ){

            $values = array();

            foreach ( $variations as $variation ){
                $attribute_keys = array_keys( $variation['attributes'] );
                foreach ( $attribute_keys as $key ){
                    if ( $key === $attribute ){
                        $values[] = $variation['attributes'][$key];
                    }
                }
            }

            $values = array_unique( $values );
            wp_set_object_terms( $post_id, $values, 'pa_' . $attribute, true);
        }

        $product_attributes_data = array();

        foreach ( $available_attributes as $attribute ){
            
            $product_attributes_data[ 'pa_'.$attribute ] = array(
                'name'         => 'pa_'.$attribute,
                'value'        => '',
                'is_visible'   => '1',
                'is_variation' => '1',
                'is_taxonomy'  => '1'
            );
        }

        update_post_meta( $post_id, '_product_attributes', $product_attributes_data );
        $this->log->warning( sprintf( 
    		"Product attributes for product_id: %s</strong> created successfully.", $post_id )
   		);
    }

    /**
     * Insert product variations
     * @param  int $post_id
     * @param  array $variations
     * @return void
     */
    public function insert_product_variations ( $post_id, $variations ){

        foreach ( $variations as $index => $variation ){
           
            $variation_post = array(
                'post_title'  => 'Variation #'.$index.' of '.count($variations).' for product#'. $post_id,
                'post_name'   => 'product-'.$post_id.'-variation-'.$index,
                'post_status' => 'publish',
                'post_parent' => $post_id,
                'post_type'   => 'product_variation',
                'guid'        => home_url() . '/?product_variation=product-' . $post_id . '-variation-' . $index
            );

            $variation_post_id = wp_insert_post( $variation_post );
            $this->log->warning( sprintf( 
	    		"Product variation for product_id: %s</strong> created successfully with variation_id: %s", $post_id, $index )
	   		);
            do_action( 'woocommerce_create_product_variation', $variation_post_id );
            
            foreach ( $variation['attributes'] as $attribute => $value ){
                $attribute_term = get_term_by( 'name', $value, 'pa_'.$attribute );
                update_post_meta( $variation_post_id, 'attribute_pa_'.$attribute, $attribute_term->slug );
            }

            if ( $variation['price'] ) 
                update_post_meta( $variation_post_id, '_regular_price', $variation['price'] );

            if ( $variation['sale_price'] && ($variation['sale_price'] != $variation['price'])){
                update_post_meta( $variation_post_id, '_sale_price', $variation['sale_price'] );
                update_post_meta( $variation_post_id, '_price', $variation['sale_price'] );
            } else{
                update_post_meta( $variation_post_id, '_price', $variation['price'] );
            }

             if($variation['sale_price'] == $variation['price'])
                delete_post_meta( $variation_post_id, '_sale_price');

            update_post_meta( $variation_post_id, '_sku', $variation['sku'] );

            update_post_meta( $variation_post_id, '_backorders', $variation['backorders'] );
            update_post_meta( $variation_post_id, '_stock', $variation['stock'] );
            update_post_meta( $variation_post_id, '_manage_stock', $variation['manage_stock'] );
            update_post_meta( $variation_post_id, '_stock_status', 'instock' );
            

            //clean_post_cache( $variation_post_id );
        }

        //wc_delete_product_transients( $variation_post_id );
    }

    /**
     * Check if product exist
     * @param  int|string $product_sku product sku
     * @return bool true if product exist, false otherwise
     */
    public function product_exist( $product_sku ){
       
        global $wpdb;

        $product_id = $wpdb->get_var( 
            $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_sku' AND meta_value = '%s' LIMIT 1", $product_sku ) 
        );

        if ( $product_id )
            return $product_id;

        return false;
    }

    /**
     * TO:DO - FIX ENCODING ( rows not in utf-8 will be ignored. )
     * Read and parse csv file 
     * @param  string $file_name csv file name to read
     * @return array
     */
	public function read_csv( $file_name ){

        if ( !file_exists( $file_name ) ) 
            return;

		$reader = League\Csv\Reader::createFromPath( $file_name );
        $reader->setOutputBOM( League\Csv\Reader::BOM_UTF8 );
        $reader->setDelimiter( self::$csv_delimiter );

		return $reader;
	}
}

endif;

if ( is_admin() )
	new WooCommerce_Csv_Import;