<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
if ( ! is_active_sidebar( 'sidebar-filter ' ) ) {
	return;
}
?>

<div class="sidebar-filter">
	<?php dynamic_sidebar('sidebar-filter'); ?>
</div><!-- #secondary -->