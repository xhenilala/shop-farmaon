<?php

namespace App\Http;

use App\Base\Ajax;
use Symfony\Component\HttpFoundation\Request;

class Newsletter extends Ajax
{
    public $protection = ['public', 'private'];
    
    public function handle(Request $request) {

        $res = $this->register([
            'email_address'     => $request->get('email_address'),
            'terms'             => $request->get('terms')
        ]);

        return json($res)->status(200);
    }

    /**
     * Register email to database
     *
     * @param array $data
     * @return integer
     */
    public function register( $data ){
        
        global $wpdb;

        $form_data = wp_parse_args( $data, [
            'email_address' => false,
            'terms'         => false,
        ]);

        if( !$form_data['email_address'] ){
            return [
                'success' => false,
                'message' => 'Indirizzo email non inserito'
            ];
        }

        if ( !filter_var( $form_data['email_address'], FILTER_VALIDATE_EMAIL ) ) {
            return [
                'success' => false,
                'message' => 'Indirizzo email non valido'
            ];
        }

        if( !$form_data['terms'] ){
            return [
                'success' => false,
                'message' => 'Condizioni d’uso non accettati'
            ];
        }

        if( $this->record_exists( $form_data['email_address'] ) ){
            return [
                'success' => false,
                'message' => 'Indirizzo email esiste'
            ];
        }

        $id = $wpdb->insert(
            $wpdb->prefix. 'newsletter',
            [                
                'email_address' => $form_data['email_address'],
                'ip_address' 	=> self::get_user_ip(),
                'u_agent' 		=> self::get_u_agent()
            ]
        );

        if( $id ){
            return [
                'success' => true,
                'message' => 'Subito per te uno sconto del 10% sul tuo primo ordine online! Non perdere le iniziative e gli sconti dedicati ai nuovi iscritti.'
            ];
        }

        return [
            'success' => false,
            'message' => 'La registrazione non è andata a buon fine'
        ];

    }

    /**
     * Check if record exists in database
     *
     * @param string $email
     * @return boolean
     */
    public function record_exists( $email ){
        
        global $wpdb;

        $user = $wpdb->get_results(
            sprintf(
                'select *, count(*) as cnt from `%s` where email_address = "%s"',
                $wpdb->prefix. 'newsletter',
                $email
            )
        );

        return $user[0]->cnt;
    }

    /**
	 * Get user ip
	 * @return string user ip
	 */
	public static function get_user_ip(){
		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
        
	}

	/**
	 * Get user agent
	 * @return string user agent
	 */
	public static function get_u_agent(){
		return esc_html( $_SERVER['HTTP_USER_AGENT'] );
    }
    
    /**
	 * Clean user iput
	 * @param  string $input input to cleanup
	 * @return string cleaned input
	 */
	public static function clean_input( $input ){
		
		$input = wp_specialchars_decode( 
			esc_html( stripslashes( strip_tags( $input ) ) ), ENT_QUOTES 
		);

		return trim($input);
    }
    
}