<?php

/**
 * 
 * 
 * Enter your helpers here
 * 
 * 
 */

if (!function_exists('logo')) {
    function logo()
    {
        return App\Site::logo();
    }
}

if (!function_exists('logo_url')) {
    function logo_url()
    {
        $logo = App\Site::logo();
        return $logo ? $logo['url'] : '';
    }
}

if (!function_exists('logo_img')) {
    function logo_img()
    {
        return App\Site::logo_img();
    }
}

if (!function_exists('svg_logo')) {
    function svg_logo($attributes = [], \Closure $callback = null)
    {
        echo App\Site::svg_logo($attributes, $callback);
    }
}
if (!function_exists('the_post_img_url')) {
    /**
     * Echo post img url
     */
    function the_post_img_url()
    {
        echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
    }
}

if (!function_exists('is_current_term')) {
    /**
     * Helper function to check if term is current
     * @param array $term
     * @param int $current_term
     * @return bool
     */
    function is_current_term($term, $current_term)
    {
        if (@$term['term_id'] == $current_term) {
            return true;
        }

        $next_terms = $term['children'];
        if (!is_array($next_terms)) {
            return false; // the term is not current and has no children
        }

        $current = array_reduce($next_terms, function ($current, $next_term) use ($current_term) {
            return is_current_term($next_term, $current_term) ? true : $current;
        }, false);
        return $current;
    }
}


if (!function_exists('is_category_selected')) {
    /**
     * Helper function to check if category is selected
     * @param array $level
     * @return bool
     */
    function is_category_selected($level)
    {
        $queried_object = get_queried_object();

        if (!is_a($queried_object, 'WP_Term')) {
            return false;
        }

        return is_current_term($level, $queried_object->term_id);
    }
}
 

function get_mobile_menu_html() {
    ob_start();
    render_mobile_menu();
    $menu_tree_html = ob_get_contents();
    ob_clean();
    return $menu_tree_html;
}

function render_mobile_menu() {
    $main_menu_tree = \App\MainMenu::init()->get_tree();
    ?>
        <div class="js-mobile-navigation mobile-navigation">
            <div class="mobile-navigation__container">
               
                <div class="mobile-navigation__body">
                    <?php render_mobile_menu_level($main_menu_tree); ?>
                </div>
            </div>
        </div>
    <?php
}

function render_mobile_menu_level($items, $level = 0) {
    if(!$items) { return; }
    $class = '';
    $link_class = '';
    $link_item_class = '';
    
    ?>
        
        <ul class="js-mobile-navigation-nav nav mobile-navigation__nav flex-column level-<?php echo $level; ?> <?php echo $class; ?>">
            <?php foreach($items as $ch): $item = collect($ch); $has_children = (bool) $item->get('children'); ?>
            <?php $active_link_class = is_category_selected($ch) ? ' is-selected': ''; ?>
            <?php $active_link_item_class = is_category_selected($ch) ? ' show': ''; ?>
            <li class="nav-item mobile-navigation__item <?php echo $link_item_class; echo $active_link_item_class; ?> <?php echo $has_children ? 'has-children' : ''; ?>" itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement">
                <a
                    data-level="<?php echo $level; ?>"
                    title="<?php echo $item->get('name') ?>"
                    class="<?php echo $has_children ? 'js-mobile-navigation-link' : '' ?> nav-link mobile-navigation__link <?php echo $link_class; echo $active_link_class; ?>"
                    href="<?php echo $item->get('link') ?>">
                    <span><?php echo $item->get('name') ?></span>
                    <?php if($has_children): ?><i class="fas mobile-navigation__icon"></i><?php endif; ?>
                </a>
                <?php render_mobile_menu_level($item->get('children'), $item->get('level') + 1)?>
            </li>
            <?php endforeach; ?>
        </ul>
        
    <?php
}