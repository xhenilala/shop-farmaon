<?php 
namespace App\Services\ProductImporter;

use App\Base\Singleton;


class Importer extends Singleton
{

    /**
     * Csv delimiter
     * @var string
     */
    public static $csv_delimiter = ',';


    /**
     * Allow product import
     */
    const ALLOW_IMPORT = true;
    /**
     * Product column numbers
     */
    // const PRODUCT_CODE                 =0;
    // const PRODUCT_SKU                  =1;
    // const PRODUCT_SIZE                 =2;
    // const PRODUCT_COLOR                =3;
    // const PRODUCT_PATTERN         	   =4;
    // const PRODUCT_NAME           	   =5;
    // const PRODUCT_PARENT_CATEGORY      =6;
    // const PRODUCT_CATEGORY             =7;
    // const PRODUCT_PRICE        		   =8;
    // const PRODUCT_SALE_PRICE           =9;
    // const PRODUCT_SHORT_DESCRIPTION    =10;
    // const PRODUCT_DESCRIPTION          =11;
    // const PRODUCT_USAGE                =12;
    // const PRODUCT_INGREDIENTS          =13;
    // const PRODUCT_GUARANTEE            =14;
    // const PRODUCT_IMAGE_1              =15;
    // const PRODUCT_IMAGE_2              =16;
    // const PRODUCT_IMAGE_3              =17;
    // const PRODUCT_IMAGE_4              =18;
    // const PRODUCT_IMAGE_5              =19;
    // const PRODUCT_TAG                  =20;

    // NEW IMPORT
    const PRODUCT_CODE                  =0;
    const PRODUCT_SKU                   =1;
    const PRODUCT_SIZE                  =2;
    const PRODUCT_SIZE_EN               =3;
    const PRODUCT_COLOR                 =4;
    const PRODUCT_COLOR_EN              =5;
    const PRODUCT_PATTERN               =6;
    const PRODUCT_PATTERN_EN            =7;
    const PRODUCT_BRAND                 =8;
    const PRODUCT_FORMULATION           =9;
    const PRODUCT_FORMULATION_EN        =10;
    const PRODUCT_SKINTYPE              =11;
    const PRODUCT_SKINTYPE_EN           =12;
    const PRODUCT_HAIRTYPE              =13;
    const PRODUCT_HAIRTYPE_EN           =14;
    const PRODUCT_SPECIFICFOR           =15;
    const PRODUCT_SPECIFICFOR_EN        =16;
    const PRODUCT_ACTION                =17;
    const PRODUCT_ACTION_EN             =18;
    const PRODUCT_SUITABLEFOR           =19;
    const PRODUCT_SUITABLEFOR_EN        =20;
    const PRODUCT_LACTOSE_GLUTEN        =21;
    const PRODUCT_LACTOSE_GLUTEN_EN     =22;
    const PRODUCT_SPF                   =23;
    const PRODUCT_SPF_EN                =24;
    const PRODUCT_NAME                  =25;
    const PRODUCT_NAME_EN               =26;
    const PRODUCT_PARENT_CATEGORY       =27;
    const PRODUCT_PARENT_CATEGORY_EN    =28;
    const PRODUCT_CATEGORY              =29;
    const PRODUCT_CATEGORY_EN           =30;
    const PRODUCT_PRICE                 =31;
    const PRODUCT_SALE_PRICE            =32;
    const PRODUCT_SHORT_DESCRIPTION     =33;
    const PRODUCT_SHORT_DESCRIPTION_EN  =34;
    const PRODUCT_DESCRIPTION           =35;
    const PRODUCT_DESCRIPTION_EN        =36;
    const PRODUCT_USAGE                 =37;
    const PRODUCT_USAGE_EN              =38;
    const PRODUCT_INGREDIENTS           =39;
    const PRODUCT_INGREDIENTS_EN        =40;
    const PRODUCT_GUARANTEE             =41;
    const PRODUCT_GUARANTEE_EN          =42;
    const PRODUCT_IMAGE_1               =43;
    const PRODUCT_IMAGE_2               =44;
    const PRODUCT_IMAGE_3               =45;
    const PRODUCT_IMAGE_4               =46;
    const PRODUCT_IMAGE_5               =47;
    const PRODUCT__TAG                  =48;
    const PRODUCT_TAG_EN                =49;
    

    

    /**
     * Author id for new post 
     */
    const PRODUCT_AUTHOR_ID = 1;


    protected function __construct() {
        

        if (is_admin()) { 
			add_action('woocommerce_csv_import_on_form_submit', array($this, 'import_products'));
        }
        
        if ( isset($_GET['debug'])) {
            add_filter( 'init', function(){
               $test_cat =  $this->get_specific_terms('Bende dhe garza sterile','Higjienë');
               dd($test_cat);
            } );   
        }
    
		// add_action('init',function(){ return;
		// 	$parent_cat = $this->get_specific_terms( 'Pelena dhe Letra të Lagura', 'Bebe dhe Nëna');
			
			
		// 	wp_set_object_terms( 1577, $parent_cat, 'product_cat' );
		// 	dd($parent_cat);
		// });
    }
    public function log( $message ){
        $logger = wc_get_logger();
        $channel = sprintf( '%s-%s', 'import', date('d-m-Y') );
        $logger->debug( $message, [ 'source' => $channel ] );
    }
    
	

	public function get_specific_terms( $term, $parent ) {
		
		$term = trim($term);
		$parent_term = get_term_by( 'name', $parent, 'product_cat' );
		// dump($parent_term);
		if( $parent_term ) {
			
            $terms = get_term_children( $parent_term->term_id, 'product_cat' );
            // dump($terms);
			foreach( $terms as $term_id ) {
				$current_term = get_term(  $term_id, 'product_cat' );
				//dump($current_term);
				if( $term == $current_term->name ){
					
					$parents = get_ancestors( $term_id, 'product_cat', 'taxonomy');
					if ( $parents ){
						return array_merge( $parents, [$term_id] );
					}
					return [$term_id];
				}
			}
		}
	}

   
    /**
     * Create product gallery
     * @param  int $post_id
     * @param  array $images
     * @return void
     */
    public function create_product_gallery( $post_id, $images ){
        
        if ( !$post_id ) {
            return new WP_Error( 'error', __( "Missing post id, or post does not exist.", "woocommerce_csv_import" ) );
        }

        if( !is_array( $images ) || empty( $images ) ){
            
            $this->$log('Product gallery', sprintf( "Cannot create gallery for:  %s no images found", $post_id ));
            
            return new WP_Error( 'error', __( "No images provided.", "woocommerce_csv_import" ) );
        }

        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        $image_ids = array();
        foreach( $images as $image ){
            $id = $this->_upload_file( $image );
            $image_ids[] = $id;
        }

        $_return = $image_ids;

        if ( !empty( $image_ids ) ) {
          
			 // Set wordpress featured image
			 set_post_thumbnail( $post_id, $image_ids[0] );
			 if( count($image_ids ) > 1 ){
				unset($image_ids[0]);
				// Set woocommerce gallery
				update_post_meta( $post_id , '_product_image_gallery', implode( ',' , $image_ids ) );
			 }
            
           
        }
        $_return = $this->clean_img_ids( $_return );
        return $_return;
    }
    
    public function clean_img_ids( $ids ){
        if( ! $ids ){
            return false;
        }
        $_return = [];
        foreach( $ids as $id ){
            if( $id ){
                $_return[] = $id;
            }
        }
        return $_return;
    }

    /**
     * Upload file to wordpress
     * @param  string $file
     * @return int - attachment id on success, false otherwise
     */
    public function _upload_file( $file ){
        if ( !$file ){
			return;
		}
        $filename = basename( $file );
        $upload_file = wp_upload_bits( $filename, null, @file_get_contents( $file ) );

        if ( !$upload_file['error'] ) {
            $parent_post_id = 0;
            $wp_filetype = wp_check_filetype( $filename, null );
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_parent' => $parent_post_id,
                'post_title' => preg_replace( '/\.[^.]+$/', '', $filename ),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $parent_post_id );            
            if ( !is_wp_error( $attachment_id ) ) {
                $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
                wp_update_attachment_metadata( $attachment_id,  $attachment_data );
                return $attachment_id;
            }
        }

        return false;
    }

   /**
     * Upload Csv file
     * @return int - wp attachment id on success, false otherwise
     */
    public static function upload_file()
    {
        
            // Security check.
        if (isset($_POST['woocommerce_csv_import_nonce'])
            && wp_verify_nonce($_POST['woocommerce_csv_import_nonce'], 'upload_csv_file')) {

            require_once(ABSPATH . 'wp-admin/includes/image.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/media.php');

                // Upload file, get attachment id.
            $attachment_id = media_handle_upload('upload_csv_file', null);

            if (!is_wp_error($attachment_id))
                return $attachment_id;
        }

    return false;
    }
    
    /**
     * Extract product details
     * @param  array $row ( csv row )
     * @return array
     */
    public function extract_product_row( $row ){
        
        // dump($row);die;

        $product = array(
            'code'                  => apply_filters( 'product_code' , $row[self::PRODUCT_CODE] ),
            'sku'                   => apply_filters( 'product_sku' , $row[self::PRODUCT_SKU] ), 
            'size'                  => apply_filters( 'product_size' , $row[self::PRODUCT_SIZE] ),
            'size_en'               => apply_filters( 'product_size_en' , $row[self::PRODUCT_SIZE_EN] ),
            'color'                 => apply_filters( 'product_color' , $row[self::PRODUCT_COLOR] ),
            'color_en'              => apply_filters( 'product_color_en' , $row[self::PRODUCT_COLOR_EN] ),
            'pattern'               => apply_filters( 'product_pattern' , $row[self::PRODUCT_PATTERN] ),
            'pattern_en'            => apply_filters( 'product_pattern_en' , $row[self::PRODUCT_PATTERN_EN] ),
            'brand'                 => apply_filters( 'product_brand' , $row[self::PRODUCT_BRAND] ),
            'formulation'           => apply_filters( 'product_formulation' , $row[self::PRODUCT_FORMULATION] ),
            'formulation_en'        => apply_filters( 'product_formulation_en' , $row[self::PRODUCT_FORMULATION_EN] ),
            'skintype'              => apply_filters( 'product_skintype' , $row[self::PRODUCT_SKINTYPE] ),
            'skintype_en'           => apply_filters( 'product_skintype_en' , $row[self::PRODUCT_SKINTYPE_EN] ),
            'hairtype'              => apply_filters( 'product_hairtype' , $row[self::PRODUCT_HAIRTYPE] ),
            'hairtype_en'           => apply_filters( 'product_hairtype_en' , $row[self::PRODUCT_HAIRTYPE_EN] ),
            'specificfor'           => apply_filters( 'product_specificfor' , $row[self::PRODUCT_SPECIFICFOR] ),
            'specificfor_en'        => apply_filters( 'product_specificfor_en' , $row[self::PRODUCT_SPECIFICFOR_EN] ),
            'action'                => apply_filters( 'product_action' , $row[self::PRODUCT_ACTION] ),
            'action_en'             => apply_filters( 'product_action_en' , $row[self::PRODUCT_ACTION_EN] ),
            'suitablefor'           => apply_filters( 'product_suitablefor' , $row[self::PRODUCT_SUITABLEFOR] ),
            'suitablefor_en'        => apply_filters( 'product_suitablefor_en' , $row[self::PRODUCT_SUITABLEFOR_EN] ),
            'lactose_gluten'        => apply_filters( 'product_lactose_gluten' , $row[self::PRODUCT_LACTOSE_GLUTEN] ),
            'lactose_gluten_en'     => apply_filters( 'product_lactose_gluten_en' , $row[self::PRODUCT_LACTOSE_GLUTEN_EN] ),
            'spf'                   => apply_filters( 'product_spf' , $row[self::PRODUCT_SPF] ),
            'spf_en'                => apply_filters( 'product_spf_en' , $row[self::PRODUCT_SPF_EN] ),
            'name'                  => apply_filters( 'product_name' , $row[self::PRODUCT_NAME] ), 
            'name_en'               => apply_filters( 'product_name_en' , $row[self::PRODUCT_NAME_EN] ), 
            'categories'            => $this->get_specific_terms( $row[self::PRODUCT_CATEGORY], $row[self::PRODUCT_PARENT_CATEGORY] ),
            'categories'            => $this->get_specific_terms( $row[self::PRODUCT_CATEGORY_EN], $row[self::PRODUCT_PARENT_CATEGORY_EN] ),
            'price'                 => apply_filters( 'product_price' , $row[self::PRODUCT_PRICE] ),
            'sale_price'            => apply_filters( 'product_price' , $row[self::PRODUCT_SALE_PRICE] ),
            'short_description'     => apply_filters( 'product_short_description' , $row[self::PRODUCT_SHORT_DESCRIPTION] ),
            'short_description_en'  => apply_filters( 'product_short_description_en' , $row[self::PRODUCT_SHORT_DESCRIPTION_EN] ),
            'description'           => apply_filters( 'product_description' , $row[self::PRODUCT_DESCRIPTION] ),
            'description_en'        => apply_filters( 'product_description_en' , $row[self::PRODUCT_DESCRIPTION_EN] ),
            'usage'                 => apply_filters( 'product_usage' , $row[self::PRODUCT_USAGE] ),
            'usage_en'              => apply_filters( 'product_usage_en' , $row[self::PRODUCT_USAGE_EN] ),
            'ingredients'           => apply_filters( 'product_ingredients' , $row[self::PRODUCT_INGREDIENTS] ),
            'ingredients_en'        => apply_filters( 'product_ingredients_en' , $row[self::PRODUCT_INGREDIENTS_EN] ),
            'guarantee'             => apply_filters( 'product_guarantee' , $row[self::PRODUCT_GUARANTEE] ),
            'guarantee_en'          => apply_filters( 'product_guarantee_en' , $row[self::PRODUCT_GUARANTEE_EN] ),
            'images'       		    =>  [$row[self::PRODUCT_IMAGE_1],
									    $row[self::PRODUCT_IMAGE_2],
									    $row[self::PRODUCT_IMAGE_3],
									    $row[self::PRODUCT_IMAGE_4],
                                        $row[self::PRODUCT_IMAGE_5]],
            'tag'                   => apply_filters( 'product_tag', $row[self::PRODUCT_TAG] ),
            'tag_en'                => apply_filters( 'product_tag_en', $row[self::PRODUCT_TAG_EN] ),
            
            );

        array_walk( $product, array( $this, 'trim_values' ) );
        return $product;
    }

    public function trim_values( $val ){

        if ( is_string( $val ) )
            return trim( $val );

        return $val;
    }

    /**
     * Import porducts
     * @return void
     */
    public function import_products( $file_name = false ){
        set_time_limit( 0 );

        if ( !isset( $_POST['upload_csv'] ) && !$file_name )
    		return;
            
        if ( !$file_name ) {
        	$attachment_id = self::upload_file();
        	if ( !$attachment_id ){
        		echo __( 'Missing attachment id', 'woocommerce_csv_import' );
        		return;
        	}

        	$file_name = get_attached_file( $attachment_id );
        }

        $product_rows = $this->read_csv( $file_name );       

    	if ( !$product_rows ){
    		echo __( 'This is not a valid csv file', 'woocommerce_csv_import' );
    		return;
    	}
        $main_product_id  = 0;

    	foreach( $product_rows as $row_num => $product_row ){
            set_time_limit( 0 );
            // exclude header
            if ( $row_num > 0 ){
                
                // extract fields
                $product = $this->extract_product_row( $product_row );
//                dump($product);die;
               
                // check if product code exist, 
                if ( $product['code'] ) {
                    

                    $products[ $product['code'] ][ 'data' ] = $product;
                    $products[ $product['code'] ][ 'variations' ][ $product['sku'] ] = $product;
                    
                    if($main_product_id !== $product['code']){
                        //dump($main_product_id,$product['code']);
                        $products[ $product['code'] ]['details'] = [
                            'Përshkrimi' => $product['description'],
                            'Përdorimi'  => $product['usage'],
                            'Përbërësit' => $product['ingredients'],
                            'Garancia'   => $product['guarantee'],
                        ];
                        $products[ $product['code'] ]['details_en'] = [
                            'Description' => $product['description_en'],
                            'Usage'  => $product['usage_en'],
                            'Ingredients' => $product['ingredients_en'],
                            'Guarantee'   => $product['guarantee_en'],
                        ];
                        $products[ $product['code'] ]['short_description'] = $product['short_description'];
                        $products[ $product['code'] ]['short_description_en'] = $product['short_description_en'];
                        $products[ $product['code'] ]['tag'] = $product['tag'];
                        $products[ $product['code'] ]['tag_en'] = $product['tag_en'];
                        $main_product_id = $product['code'];
                    }
                }
                //dd($products);
            }
        }  

        $products_formated = array();
        
        $i = 0; 
        foreach ( $products as $key => $product ) {

            if ( is_array( $product['variations'] ) ) {
                $variation_images = [];

                $j = 0; 
                foreach ( $product['variations'] as $e => $product_variation ) {

                  
                    $variation_images[] = ( array_key_exists( 0,  $product_variation['images'] ) ) ? $product_variation['images'][0] : false;

                    $variations[$i][$j] = array(
                        'attributes' => array(
                            'size'  	    => $product_variation['size'],
                            'color'         => $product_variation['color'],
                            'pattern'         => $product_variation['pattern'],
                            'brand'	        => $product_variation['brand'],
                            'formulation'	=> $product_variation['formulation'],
                            'skintype'	    => $product_variation['skintype'],
                            'hairtype'	    => $product_variation['hairtype'],
                            'specificfor'	=> $product_variation['specificfor'],
                            'action'	    => $product_variation['action'],
                            'suitablefor'	=> $product_variation['suitablefor'],
                            'lactose_gluten'=> $product_variation['lactose_gluten'],
                            'spf'	        => $product_variation['spf'],
                        ),
                        'price'         =>  $product_variation['price'],
                        'sale_price'    =>  $product_variation['sale_price'],
                        'sku'           =>  $product_variation['sku'],
					); 
					
                $j++;
                }
            }
			
			$attributes = [
				'size', 'color', 'pattern', 'brand', 'formulation', 'skintype', 'hairtype', 'specificfor', 'action', 'suitablefor', 'lactose_gluten', 'spf'
            ];
            $available_attributes = false;
			foreach($attributes as $attribute){
				if($product_variation[$attribute]){
					$available_attributes[] = $attribute;	
				}
			}
			
			
			$products_formated[$i] = array(
				'code'          => $product['data']['code'],
				'sku'           => $product['data']['sku'],
				'available_attributes'  => $available_attributes,
                'name'          => $product['data']['name'],
                'name_en'          => $product['data']['name_en'],
				'categories'    => $product['data']['categories'],
                'price'         => $product['data']['price'],
                'sale_price'    => $product['data']['sale_price'],
                'short_description' => $product['short_description'],
                'short_description_en' => $product['short_description_en'],
                'product_details' => $product['details'] ,
                'product_details_en' => $product['details_en'] ,
				'images'		  =>  $product['data']['images'],
                'variations'      => (count($variations[$i]) > 1 ) ? $variations[$i] : false,
                'tag'              => $product['tag'],
                'tag_en'              => $product['tag_en'],
                'variation_images' => $variation_images,
			
            );
            // dump($products_formated);
			$available_attributes = false;
            
            if ( self::ALLOW_IMPORT && !self::product_exist( $products_formated[$i]['code'] ) ) {
				// dump($products_formated[$i]);
                $this->insert_product( $products_formated[$i] );
                $this->log('Product insert', sprintf("Inserting product :  %s", $products_formated[$i]['code']));
               
            } else {
                $this->log('Product exists', sprintf("Product :  %s", $products_formated[$i]['code']));
            }

        $i++;
        }
    }


    /**
     * Insert product
     * @param  array $product_data 
     * @return void
     */
    public function insert_product ( $product_data ){

        $post = array(
            //'post_author'  => self::PRODUCT_AUTHOR_ID,
            'post_content' => '',
            'post_excerpt' => $product_data['short_description'],
            'post_status'  => 'publish',
            'post_title'   => $product_data['name'], 
            'post_type'    => 'product'
        );
		
        $post_id = wp_insert_post( $post );
		
        if ( !$post_id )
            return false;

        $this->log( sprintf( "Product :  %s inserted successfully with post_id: %d</strong>", $product_data['code'], $post_id));
        
        update_post_meta( $post_id , '_sku', $product_data['sku'] );
        update_post_meta( $post_id , '_visibility' , 'visible' );

        if ( !empty( $product_data['sale_price'] && $product_data['price'] != $product_data['sale_price'] ) ) {
            update_post_meta( $post_id , '_sale_price', $product_data['sale_price'] );//check if has sale price
            update_post_meta( $post_id , '_regular_price', $product_data['price'] );
            update_post_meta( $post_id , '_price', $product_data['sale_price'] );
        }else {
            update_post_meta( $post_id , '_regular_price', $product_data['price'] );
            update_post_meta( $post_id , '_price', $product_data['price'] );
        }
        
        if ( is_array($product_data['categories']) && !empty( $product_data['categories'] ) ) {
           
			wp_set_object_terms( $post_id, $product_data['categories'], 'product_cat' ); 
			
        }
        
        // insert product tag
        if ( $product_data['tag'] && !empty( $product_data['tag']) ) {
            wp_set_post_terms( $post_id, $product_data['tag'], 'product_tag', false );
            update_term_meta($post_id, 'name_en', $product_data['tag_en']);
        }

        if ($product_data['name_en'] && !empty($product_data['name_en'])) {
            update_field('post_title_en', $product_data['name_en'], $post_id);
        }
        // product data in albanian
        $product_details = $product_data['product_details'];
        if ( $product_details ) {
            
            foreach( $product_details as $key => $value ){
                if( $value ) {
                    $row = array(
                        'title' => $key,
                        'description' => $value,
                        ); 
           
                    add_row( 'specifics', $row, $post_id );	
                }
            }
            
        }

        // product data in english
        $product_details = $product_data['product_details'];
        // dump($product_details);
        if ( $product_details ) {
            
            foreach( $product_details as $key => $value ){
                if( $value ) {
                    $row = array(
                        'field_5dcd70f42aaea' => $key,
                        'field_5dcd70fb2aaeb' => $value,
                        ); 
           
                    add_row( 'field_5dcd70c32aae9', $row, $post_id );	
                }
            }
            
        }

       // $product_images = $product_data['images'];
        $variation_images = $product_data['variation_images'];
        // $product_images = $product_data['images'];
        if ( is_array( $variation_images ) ){
            $image_ids = $this->create_product_gallery( $post_id, $variation_images );
        }

        
        
		if($product_data['variations']){
			wp_set_object_terms( $post_id, 'variable', 'product_type' );
            $this->insert_product_attributes( $post_id, $product_data['available_attributes'], $product_data['variations'] );
            
           // $variation_images = $product_data['variation_images'];
			$this->insert_product_variations( $post_id, $product_data['variations'], $image_ids );
		}

        

        
        \WC_Product_Variable::sync_stock_status( $post_id );
        \WC_Product_Variable::sync( $post_id );


        wc_delete_product_transients( $post_id );
    }

    /**
     * Insert product attributes
     * @param  int $post_id
     * @param  array $available_attributes
     * @param  array $variations
     * @return void
     */
    public function insert_product_attributes ( $post_id, $available_attributes, $variations ){
        //dump( $image_ids);die;
        if ( !$available_attributes ){
			return;
		}
        foreach ( $available_attributes as $attribute ){

            $values = array();

            foreach ( $variations as $variation ){
				if( !$variation ){
					continue;
				}
                $attribute_keys = array_keys( $variation['attributes'] );
                foreach ( $attribute_keys as $key ){
                    if ( $key === $attribute ){
                        $values[] = $variation['attributes'][$key];
                    }
                }
            }

			$values = array_unique( $values );
            wp_set_object_terms( $post_id, $values, 'pa_' . $attribute, true);
        }

        $product_attributes_data = array();

        foreach ( $available_attributes as $attribute ){
            
            $product_attributes_data[ 'pa_'.$attribute ] = array(
                'name'         => 'pa_'.$attribute,
                'value'        => '',
                'is_visible'   => '1',
                'is_variation' => '1',
                'is_taxonomy'  => '1'
            );
        }
        update_post_meta( $post_id, '_product_attributes', $product_attributes_data );

        $this->log('Attirbute insert', sprintf("Product attributes for product_id: %s</strong> created successfully.", $post_id ));
    }

    /**
     * Insert product variations
     * @param  int $post_id
     * @param  array $variations
     * @return void
     */
    public function insert_product_variations ( $post_id, $variations ,$image_ids){
        //dump($image_ids);die;
        $i = 0;
        foreach ( $variations as $index => $variation ){
           
            $variation_post = array(
                'post_title'  => 'Variation #'.$index.' of '.count($variations).' for product#'. $post_id,
                'post_name'   => 'product-'.$post_id.'-variation-'.$index,
                'post_status' => 'publish',
                'post_parent' => $post_id,
                'post_type'   => 'product_variation',
                'guid'        => home_url() . '/?product_variation=product-' . $post_id . '-variation-' . $index
            );

            $variation_post_id = wp_insert_post( $variation_post );
           
            // error in here check
            if ( $image_ids ) {
                // dd($image_ids);
                if( array_key_exists( $i, $image_ids ) && $image_ids[$i] ){
                    update_post_meta($variation_post_id,'_thumbnail_id', $image_ids[$i] );
                }
            }
           
            

            $this->log( 'Variation insert', sprintf( "Product variation for product_id: %s</strong> created successfully with variation_id: %s", $post_id, $index ) );

            do_action( 'woocommerce_create_product_variation', $variation_post_id );
            
            foreach ( $variation['attributes'] as $attribute => $value ){
				if($value){
					$attribute_term = get_term_by( 'name', $value, 'pa_'.$attribute );
                	update_post_meta( $variation_post_id, 'attribute_pa_'.$attribute, @$attribute_term->slug );
				}
				
            }

            if ( $variation['price'] ) 
                update_post_meta( $variation_post_id, '_regular_price', $variation['price'] );

            if ( $variation['sale_price'] && ($variation['sale_price'] != $variation['price'])){
                update_post_meta( $variation_post_id, '_sale_price', $variation['sale_price'] );
                update_post_meta( $variation_post_id, '_price', $variation['sale_price'] );
            } else{
                update_post_meta( $variation_post_id, '_price', $variation['price'] );
            }

            if($variation['sale_price'] == $variation['price'])
                delete_post_meta( $variation_post_id, '_sale_price');

            update_post_meta( $variation_post_id, '_sku', $variation['sku'] );

           $i++;
        }

    }

    /**
     * Check if product exist
     * @param  int|string $product_sku product sku
     * @return bool true if product exist, false otherwise
     */
    public function product_exist( $product_sku ){
        // return false;
        global $wpdb;

        $product_id = $wpdb->get_var( 
            $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_sku' AND meta_value = '%s' LIMIT 1", $product_sku ) 
        );

        if ( $product_id )
            return $product_id;

        return false;
    }

    /**
     * TO:DO - FIX ENCODING ( rows not in utf-8 will be ignored. )
     * Read and parse csv file 
     * @param  string $file_name csv file name to read
     * @return array
     */
	public function read_csv( $file_name ){
    // dd($file_name);
        if ( !file_exists( $file_name ) ) 
            return;

		$reader = \League\Csv\Reader::createFromPath( $file_name );
        $reader->setOutputBOM( \League\Csv\Reader::BOM_UTF8 );
        $reader->setDelimiter( self::$csv_delimiter );
       
		return $reader;
	}

}