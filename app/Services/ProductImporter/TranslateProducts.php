<?php
namespace App\Services\ProductImporter;

use App\Base\Singleton;
use League\Csv\Reader;

class TranslateProducts extends Singleton
{
    const PRODUCT_TRANSLATIONS_FLE = 'translate_products_farmaon.csv';
    const CSV_DELIMITER = ',';




    public function __construct()
    {
        if (isset($_GET['translate_products'])) {
            add_action('init', [$this, 'translate_products'], 90);
        }
    }

    public function log($message)
    {
        $logger = wc_get_logger();
        $channel = sprintf('%s-%s', 'translate-products', date('d-m-Y'));
        $logger->debug($message, [ 'source' => $channel ]);
    }
    

    private function get_translations_csv_file()
    {
        return wp_upload_dir()['basedir'] . '/' . self::PRODUCT_TRANSLATIONS_FLE;
    }

    public function read_csv_file()
    {
        $file = $this->get_translations_csv_file();
        if (!$file) {
            return;
        }
        $reader = Reader::createFromPath($file);
        $reader->setOutputBOM(Reader::BOM_UTF8);
        $reader->setDelimiter(self::CSV_DELIMITER);
        return $reader;
    }



    public function translate_products()
    {
        $data = $this->read_csv_file();
        foreach ($data as $d) {
            $sku = $d[0];
            $product_name = $d[3];
            $product_short_description = $d[4];
            $product_description = $d[5];
            $product_usage = $d[6];
            $product_ingredients = $d[7];
            $product_guarantee = $d[8];
            $product_id = $this->get_product_id($sku);
            // TODO - Translate repeater fields / test this
            $product_details_en = [
                'Description' => $product_name,
                'Usage'  => $product_usage,
                'Ingredients' => $product_ingredients,
                'Guarantee'   => $product_guarantee,
            ];
            // dd($product_details_en);
            if ($product_details_en) {
                // dd($product_details_en, $product_id);
                foreach ($product_details_en as $key => $value) {
                    // dd($key, $value);
                    if ($value) {
                        $row = array(
                            'field_5dcd70f42aaea' => $key,
                            'field_5dcd70fb2aaeb' => $value,
                            );
                        // dd($row);
                        add_row('field_5dcd70c32aae9', $row, $product_id);
                    }
                    
                }
            }
            if ($product_id) {
                    update_field('post_title_en', $product_name, $product_id);
                    update_field('post_excerpt_en', $product_short_description, $product_id);
                $this->log(sprintf("Translated Product with ID: %s ", $product_id));
            }
        }
    }

    public function get_product_id($sku)
    {
        global $wpdb;
        $product_id = $wpdb->get_var(
            $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_sku' AND meta_value LIKE '%%%s' ", $sku)
        );
        if ($product_id) {
            return $product_id;
        }
        return false;
    }
}
