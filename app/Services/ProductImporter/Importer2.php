<?php 
namespace App\Services\ProductImporter;

use App\Base\Singleton;


class Importer extends Singleton
{

    /**
     * Csv delimiter
     * @var string
     */
    public static $csv_delimiter = ',';


    /**
     * Allow product import
     */
    const ALLOW_IMPORT = true;

    /**
     * Product column numbers
     */
    const PRODUCT_CODE                 =0;
    const PRODUCT_SKU                  =1;
    const PRODUCT_SIZE                 =2;
    const PRODUCT_COLOR                =3;
    const PRODUCT_PATTERN         	   =4;
    const PRODUCT_NAME           	   =5;
    const PRODUCT_PARENT_CATEGORY      =6;
    const PRODUCT_CATEGORY             =7;
    const PRODUCT_PRICE        		   =8;
    const PRODUCT_SALE_PRICE           =9;
    const PRODUCT_SHORT_DESCRIPTION    =10;
    const PRODUCT_DESCRIPTION          =11;
    const PRODUCT_USAGE                =12;
    const PRODUCT_INGREDIENTS          =13;
    const PRODUCT_IMAGE_1              =14;
    const PRODUCT_IMAGE_2              =15;
    const PRODUCT_IMAGE_3              =16;
    const PRODUCT_IMAGE_4              =17;
    const PRODUCT_IMAGE_5              =18;
    const PRODUCT_TAG                  =19;

    

    /**
     * Author id for new post 
     */
    const PRODUCT_AUTHOR_ID = 1;


    protected function __construct() {
        

        if (is_admin()) { 
			add_action('woocommerce_csv_import_on_form_submit', array($this, 'import_products'));
        }
        
    
		// add_action('init',function(){ return;
		// 	$parent_cat = $this->get_specific_terms( 'Pelena dhe Letra të Lagura', 'Bebe dhe Nëna');
			
			
		// 	wp_set_object_terms( 1577, $parent_cat, 'product_cat' );
		// 	dd($parent_cat);
		// });
    }
    public function log( $message ){
        $logger = wc_get_logger();
        $channel = sprintf( '%s-%s', 'import', date('d-m-Y') );
        $logger->debug( $message, [ 'source' => $channel ] );
    }
    
	

	public function get_specific_terms( $term, $parent ) {
		
		$parent_term = get_term_by( 'name', $parent, 'product_cat' );
		// dump($parent_term);
		if( $parent_term ) {
			
            $terms = get_term_children( $parent_term->term_id, 'product_cat' );
            
			foreach( $terms as $term_id ) {
				$current_term = get_term(  $term_id, 'product_cat' );
				
				if( $term == $current_term->name ){
					
					$parents = get_ancestors( $term_id, 'product_cat', 'taxonomy');
					if ( $parents ){
						return array_merge( $parents, [$term_id] );
					}
					return [$term_id];
				}
			}
		}
	}

   
    /**
     * Create product gallery
     * @param  int $post_id
     * @param  array $images
     * @return void
     */
    public function create_product_gallery( $post_id, $images ){
        
        if ( !$post_id ) {
            return new WP_Error( 'error', __( "Missing post id, or post does not exist.", "woocommerce_csv_import" ) );
        }

        if( !is_array( $images ) || empty( $images ) ){
            
            $this->$log('Product gallery', sprintf( "Cannot create gallery for:  %s no images found", $post_id ));
            
            return new WP_Error( 'error', __( "No images provided.", "woocommerce_csv_import" ) );
        }

        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        $image_ids = array();
        foreach( $images as $image ){
            $image_ids[] = $this->_upload_file( $image );
        }

        if ( !empty( $image_ids ) ) {
			 // Set wordpress featured image
			 set_post_thumbnail( $post_id, $image_ids[0] );
			 if( count($image_ids ) > 1 ){
				unset($image_ids[0]);
				// Set woocommerce gallery
				update_post_meta( $post_id , '_product_image_gallery', implode( ',' , $image_ids ) );
			 }
            
           
        }
    }

    /**
     * Upload file to wordpress
     * @param  string $file
     * @return int - attachment id on success, false otherwise
     */
    public function _upload_file( $file ){
        if ( !$file ){
			return;
		}
        $filename = basename( $file );
        $upload_file = wp_upload_bits( $filename, null, @file_get_contents( $file ) );

        if ( !$upload_file['error'] ) {
            $parent_post_id = 0;
            $wp_filetype = wp_check_filetype( $filename, null );
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_parent' => $parent_post_id,
                'post_title' => preg_replace( '/\.[^.]+$/', '', $filename ),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $parent_post_id );            
            if ( !is_wp_error( $attachment_id ) ) {
                $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
                wp_update_attachment_metadata( $attachment_id,  $attachment_data );
                return $attachment_id;
            }
        }

        return false;
    }

   /**
     * Upload Csv file
     * @return int - wp attachment id on success, false otherwise
     */
    public static function upload_file()
    {
        
            // Security check.
        if (isset($_POST['woocommerce_csv_import_nonce'])
            && wp_verify_nonce($_POST['woocommerce_csv_import_nonce'], 'upload_csv_file')) {

            require_once(ABSPATH . 'wp-admin/includes/image.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/media.php');

                // Upload file, get attachment id.
            $attachment_id = media_handle_upload('upload_csv_file', null);

            if (!is_wp_error($attachment_id))
                return $attachment_id;
        }

    return false;
    }
    
    /**
     * Extract product details
     * @param  array $row ( csv row )
     * @return array
     */
    public function extract_product_row( $row ){
        
        // dump($row);die;
        
        $product = array(
            'code'              => apply_filters( 'product_code' , $row[self::PRODUCT_CODE] ),
            'sku'               => apply_filters( 'product_sku' , $row[self::PRODUCT_SKU] ), 
            'size'              => apply_filters( 'product_size' , $row[self::PRODUCT_SIZE] ),
			'color'             => apply_filters( 'product_color' , $row[self::PRODUCT_COLOR] ),
            'pattern'           => apply_filters( 'product_pattern' , $row[self::PRODUCT_PATTERN] ),
            'name'              => apply_filters( 'product_name' , $row[self::PRODUCT_NAME] ), 
            'categories'        => $this->get_specific_terms( $row[self::PRODUCT_CATEGORY], $row[self::PRODUCT_PARENT_CATEGORY] ),
            'price'             => apply_filters( 'product_price' , $row[self::PRODUCT_PRICE] ),
            'sale_price'        => apply_filters( 'product_price' , $row[self::PRODUCT_SALE_PRICE] ),
            'short_description' => apply_filters( 'product_short_description' , $row[self::PRODUCT_SHORT_DESCRIPTION] ),
            'description'       => apply_filters( 'product_description' , $row[self::PRODUCT_DESCRIPTION] ),
            'usage'             => apply_filters( 'product_usage' , $row[self::PRODUCT_USAGE] ),
			'ingredients'       => apply_filters( 'product_ingredients' , $row[self::PRODUCT_INGREDIENTS] ),
            'images'       		=> [$row[self::PRODUCT_IMAGE_1],
									$row[self::PRODUCT_IMAGE_2],
									$row[self::PRODUCT_IMAGE_3],
									$row[self::PRODUCT_IMAGE_4],
                                    $row[self::PRODUCT_IMAGE_5]],
            'tag'               => apply_filters( 'product_tag', $row[self::PRODUCT_TAG] ),
            
            );

        array_walk( $product, array( $this, 'trim_values' ) );
        return $product;
    }

    public function trim_values( $val ){

        if ( is_string( $val ) )
            return trim( $val );

        return $val;
    }

    /**
     * Import porducts
     * @return void
     */
    public function import_products( $file_name = false ){


        if ( !isset( $_POST['upload_csv'] ) && !$file_name )
    		return;
            
        if ( !$file_name ) {
        	$attachment_id = self::upload_file();
        	if ( !$attachment_id ){
        		echo __( 'Missing attachment id', 'woocommerce_csv_import' );
        		return;
        	}

        	$file_name = get_attached_file( $attachment_id );
        }

        $product_rows = $this->read_csv( $file_name );
       

    	if ( !$product_rows ){
    		echo __( 'This is not a valid csv file', 'woocommerce_csv_import' );
    		return;
    	}
        $main_product_id  = 0;
    	foreach( $product_rows as $row_num => $product_row ){
           
            // exclude header
            if ( $row_num > 0 ){
                
                // extract fields
                $product = $this->extract_product_row( $product_row );
                // check if product code exist, 
                if ( $product['code'] ) {
                    

                    $products[ $product['code'] ][ 'data' ] = $product;
                    $products[ $product['code'] ][ 'variations' ][ $product['sku'] ] = $product;
                    
                    if($main_product_id !== $product['code']){
                        //dump($main_product_id,$product['code']);
                        $products[ $product['code'] ]['details'] = [
                            'Përshkrimi' => $product['description'],
                            'Përdorimi'  => $product['usage'],
                            'Përbërësit' => $product['ingredients'],
                        ];
                        $products[ $product['code'] ]['short_description'] = $product['short_description'];
                        $products[ $product['code'] ]['tag'] = $product['tag'];
                        $main_product_id = $product['code'];
                    }
                }
                //dd($products);
            }
        }  

        $products_formated = array();
       
        $i = 0; 
        foreach ( $products as $key => $product ) {

            if ( is_array( $product['variations'] ) ) {

                $j = 0; 
                foreach ( $product['variations'] as $e => $product_variation ) {

                    $variations[$i][$j] = array(
                        'attributes' => array(
                            'size'  	=> $product_variation['size'],
							'color'     => $product_variation['color'],
							'pattern'	=> $product_variation['pattern'],
                        ),
                        'price'         =>  $product_variation['price'],
                        'sale_price'    =>  $product_variation['sale_price'],
                        'sku'           =>  $product_variation['sku'],
					); 
					
                $j++;
                }
            }
			
			$attributes = [
				'size', 'color', 'pattern'
            ];
            $available_attributes = false;
			foreach($attributes as $attribute){
				if($product_variation[$attribute]){
					$available_attributes[] = $attribute;	
				}
			}
			
			
			$products_formated[$i] = array(
				'code'          => $product['data']['code'],
				'sku'           => $product['data']['sku'],
				'available_attributes'  => $available_attributes,
                'name'          => $product['data']['name'],
				'categories'    => $product['data']['categories'],
                'price'         => $product['data']['price'],
                'sale_price'    => $product['data']['sale_price'],
				'short_description' => $product['short_description'],
				'product_details' => $product['details'] ,
				'images'		  =>  $product['data']['images'],
                'variations'      => (count($variations[$i]) > 1 ) ? $variations[$i] : false,
                'tag'              => $product['tag'],
			
            );
            // dump($products_formated);
			$available_attributes = false;
            
            if ( self::ALLOW_IMPORT && !self::product_exist( $products_formated[$i]['code'] ) ) {
				
                $this->insert_product( $products_formated[$i] );
                $this->log('Product insert', sprintf("Inserting product :  %s", $products_formated[$i]['code']));
               
            }

        $i++;
        }
    }


    /**
     * Insert product
     * @param  array $product_data 
     * @return void
     */
    public function insert_product ( $product_data ){
        // dump($product_data);die;
        $post = array(
            //'post_author'  => self::PRODUCT_AUTHOR_ID,
            'post_content' => '',
            'post_excerpt' => $product_data['short_description'],
            'post_status'  => 'publish',
            'post_title'   => $product_data['name'], 
            'post_type'    => 'product'
        );
		
        $post_id = wp_insert_post( $post );
		
        if ( !$post_id )
            return false;

        $this->log( sprintf( "Product :  %s inserted successfully with post_id: %d</strong>", $product_data['code'], $post_id));
        
        update_post_meta( $post_id , '_sku', $product_data['code'] );
        update_post_meta( $post_id , '_visibility' , 'visible' );

        if ( !empty( $product_data['sale_price'] && $product_data['price'] != $product_data['sale_price'] ) ) {
            update_post_meta( $post_id , '_sale_price', $product_data['sale_price'] );//check if has sale price
            update_post_meta( $post_id , '_regular_price', $product_data['price'] );
            update_post_meta( $post_id , '_price', $product_data['sale_price'] );
        }else {
            update_post_meta( $post_id , '_regular_price', $product_data['price'] );
            update_post_meta( $post_id , '_price', $product_data['price'] );
        }
        
        if ( is_array($product_data['categories']) && !empty( $product_data['categories'] ) ) {
           
			wp_set_object_terms( $post_id, $product_data['categories'], 'product_cat' ); 
			
        }
        
        // insert product tag
        if ( $product_data['tag'] && !empty( $product_data['tag']) ) {
        
			wp_set_post_terms( $post_id, $product_data['tag'], 'product_tag', false );
        }

        $product_details = $product_data['product_details'];
        // dump($product_details);
        if ( $product_details ) {
            
            foreach( $product_details as $key => $value ){
                if( $value ) {
                    $row = array(
                        'title' => $key,
                        'description' => $value,
                        ); 
           
                    add_row( 'specifics', $row, $post_id );	
                }
            }
            
        }
        
		if($product_data['variations']){
			wp_set_object_terms( $post_id, 'variable', 'product_type' );
			$this->insert_product_attributes( $post_id, $product_data['available_attributes'], $product_data['variations'] );
			$this->insert_product_variations( $post_id, $product_data['variations'] );
		}

        $product_images = $product_data['images'];
        if ( is_array( $product_images ) ){
            $this->create_product_gallery( $post_id, $product_images );
        }

        
        \WC_Product_Variable::sync_stock_status( $post_id );
        \WC_Product_Variable::sync( $post_id );


        wc_delete_product_transients( $post_id );
    }

    /**
     * Insert product attributes
     * @param  int $post_id
     * @param  array $available_attributes
     * @param  array $variations
     * @return void
     */
    public function insert_product_attributes ( $post_id, $available_attributes, $variations ){
        if ( !$available_attributes ){
			return;
		}
        foreach ( $available_attributes as $attribute ){

            $values = array();

            foreach ( $variations as $variation ){
				if( !$variation ){
					continue;
				}
                $attribute_keys = array_keys( $variation['attributes'] );
                foreach ( $attribute_keys as $key ){
                    if ( $key === $attribute ){
                        $values[] = $variation['attributes'][$key];
                    }
                }
            }

			$values = array_unique( $values );
            wp_set_object_terms( $post_id, $values, 'pa_' . $attribute, true);
        }

        $product_attributes_data = array();

        foreach ( $available_attributes as $attribute ){
            
            $product_attributes_data[ 'pa_'.$attribute ] = array(
                'name'         => 'pa_'.$attribute,
                'value'        => '',
                'is_visible'   => '1',
                'is_variation' => '1',
                'is_taxonomy'  => '1'
            );
        }
        update_post_meta( $post_id, '_product_attributes', $product_attributes_data );

        $this->log('Attirbute insert', sprintf("Product attributes for product_id: %s</strong> created successfully.", $post_id ));
    }

    /**
     * Insert product variations
     * @param  int $post_id
     * @param  array $variations
     * @return void
     */
    public function insert_product_variations ( $post_id, $variations ){

	
        foreach ( $variations as $index => $variation ){
           
            $variation_post = array(
                'post_title'  => 'Variation #'.$index.' of '.count($variations).' for product#'. $post_id,
                'post_name'   => 'product-'.$post_id.'-variation-'.$index,
                'post_status' => 'publish',
                'post_parent' => $post_id,
                'post_type'   => 'product_variation',
                'guid'        => home_url() . '/?product_variation=product-' . $post_id . '-variation-' . $index
            );

            $variation_post_id = wp_insert_post( $variation_post );

            $this->log( 'Variation insert', sprintf( "Product variation for product_id: %s</strong> created successfully with variation_id: %s", $post_id, $index ) );

            do_action( 'woocommerce_create_product_variation', $variation_post_id );
            
            foreach ( $variation['attributes'] as $attribute => $value ){
				if($value){
					$attribute_term = get_term_by( 'name', $value, 'pa_'.$attribute );
                	update_post_meta( $variation_post_id, 'attribute_pa_'.$attribute, @$attribute_term->slug );
				}
				
            }

            if ( $variation['price'] ) 
                update_post_meta( $variation_post_id, '_regular_price', $variation['price'] );

            if ( $variation['sale_price'] && ($variation['sale_price'] != $variation['price'])){
                update_post_meta( $variation_post_id, '_sale_price', $variation['sale_price'] );
                update_post_meta( $variation_post_id, '_price', $variation['sale_price'] );
            } else{
                update_post_meta( $variation_post_id, '_price', $variation['price'] );
            }

            if($variation['sale_price'] == $variation['price'])
                delete_post_meta( $variation_post_id, '_sale_price');

            update_post_meta( $variation_post_id, '_sku', $variation['sku'] );

           
        }

    }

    /**
     * Check if product exist
     * @param  int|string $product_sku product sku
     * @return bool true if product exist, false otherwise
     */
    public function product_exist( $product_sku ){
       
        global $wpdb;

        $product_id = $wpdb->get_var( 
            $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_sku' AND meta_value = '%s' LIMIT 1", $product_sku ) 
        );

        if ( $product_id )
            return $product_id;

        return false;
    }

    /**
     * TO:DO - FIX ENCODING ( rows not in utf-8 will be ignored. )
     * Read and parse csv file 
     * @param  string $file_name csv file name to read
     * @return array
     */
	public function read_csv( $file_name ){
    
        if ( !file_exists( $file_name ) ) 
            return;

		$reader = \League\Csv\Reader::createFromPath( $file_name );
        $reader->setOutputBOM( \League\Csv\Reader::BOM_UTF8 );
        $reader->setDelimiter( self::$csv_delimiter );

		return $reader;
	}

}