<?php 
namespace App\Services\ProductImporter;

use App\Base\Singleton;


class Admin extends Singleton
{
    protected function __construct() {
        if (is_admin()) {
            add_action('admin_menu', array($this, 'import_products_menu'));
            //add_action('woocommerce_csv_import_on_form_submit', array($this, 'import_products'));
        }
    }
    /**
     * Callback of admin_menu
     * Adds import menu under Settings
     * @return void
     */
    public function import_products_menu()
    {
        add_submenu_page(
            'woocommerce',
            __('Import Products', 'woocommerce_csv_import'),
            __('Import Products', 'woocommerce_csv_import'),
            'manage_woocommerce',
            'woocommerce-csv-import',
            array($this, 'import_products_page')
        );
    }

    /**
     * Render csv form upload
     * @return void
     */
    public function import_products_page()
    {
        echo '<div class="wrap"><h1>' . __('Import products from csv', 'woocommerce_csv_import') . '</h1>';
        $this->upload_form();
        echo '</div>';
        do_action('woocommerce_csv_import_on_form_submit');
    }

    /**
     * Csv upload form
     * @return void
     */
    public function upload_form()
    {
        ?>
    	<form method="post" action="#" enctype="multipart/form-data">
			<input type="file" name="upload_csv_file" multiple="false"/>
			<?php wp_nonce_field('upload_csv_file', 'woocommerce_csv_import_nonce'); ?>
			<input name="upload_csv" type="submit" value="<?php _e('Upload', 'woocommerce_csv_import') ?>" class="button button-primary"/>
		</form>
		<?php

    }

    
}