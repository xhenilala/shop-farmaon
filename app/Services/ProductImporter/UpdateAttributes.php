<?php
namespace App\Services\ProductImporter;

use App\Base\Singleton;
use League\Csv\Reader;

class UpdateAttributes extends Singleton
{
    const ATTRIBUTES_FILE = 'attributes.csv';
    const CSV_DELIMITER = ',';

    //list product sku and attributes
    // product_sku	pa_brand	pa_formulation	pa_skintype	pa_hairtype	pa_specificfor	pa_action	pa_suitablefor	pa_lactose_gluten	pa_spf


    const PRODUCT_SKU = 0;
    const PRODUCT_BRAND = 1;
    const PRODUCT_FORMULATION  = 2;
    const PRODUCT_SKINTYPE = 3;
    const PRODUCT_HAIRTYPE = 4;
    const PRODUCT_SPECIFICFOR = 5;
    const PRODUCT_ACTION = 6;
    const PRODUCT_SUITABLEFOR = 7;
    const PRODUCT_LACTOSE_GLUTEN = 8;
    const PRODUCT_SPF = 9;


    public function __construct()
    {
        if (isset($_GET['update_attributes'])) {
            add_action('init', [$this, 'update_attributes'], 90);
        }

       
    }

    public function log( $message )
    {
        $logger = wc_get_logger();
        $channel = sprintf( '%s-%s', 'update-attributes', date('d-m-Y') );
        $logger->debug( $message, [ 'source' => $channel ] );
    }

    private function get_attributes_csv_file()
    {
        return wp_upload_dir()['basedir'] . '/' . self::ATTRIBUTES_FILE;
    }

    public function read_csv_file()
    {
        $file = $this->get_attributes_csv_file();
        
        if (!$file) {
            return;
        }
        $reader = Reader::createFromPath($file);
        $reader->setOutputBOM(Reader::BOM_UTF8);
        $reader->setDelimiter(self::CSV_DELIMITER);
    
        return $reader;
    }

    public function update_attributes()
    {
        $data = $this->read_csv_file();
        foreach ($data as $d) {
            // dd($d);
            $sku = $d[0];
            $product_brand = $d[1];
            $product_formulation = $d[2];
            $product_skintype = $d[3];
            $product_hairtype = $d[4];
            $product_specificfor = $d[5];
            $product_action = $d[6];
            $product_suitablefor = $d[7];
            $product_lactose_gluten = $d[8];
            $product_spf = $d[9];
            $product_id = $this->get_product_id($sku);
            // dd($sku,$product_formulation,$product_id);
            if(!empty($product_brand) || !empty($product_formulation) || !empty($product_skintype) || !empty($product_hairtype) || !empty($product_specificfor) || !empty($product_lactose_gluten) || !empty($product_spf) ) {
                wp_set_object_terms( $product_id, $product_brand, 'pa_brand', true );
                $this->log(sprintf("Attribute %s added for:  %s ", $product_brand, $product_id ));
                wp_set_object_terms( $product_id, $product_formulation, 'pa_formulation', true);
                $this->log(sprintf("Attribute %s added for:  %s ", $product_formulation, $product_id ));
                wp_set_object_terms( $product_id, $product_skintype, 'pa_skintype', true );
                $this->log(sprintf("Attribute %s added for:  %s ", $product_skintype, $product_id ));
                wp_set_object_terms( $product_id, $product_hairtype, 'pa_hairtype', true);
                $this->log(sprintf("Attribute %s added for:  %s ", $product_hairtype, $product_id ));
                wp_set_object_terms( $product_id, $product_specificfor, 'pa_specificfor', true );
                $this->log(sprintf("Attribute %s added for:  %s ", $product_specificfor, $product_id ));
                wp_set_object_terms( $product_id, $product_action, 'pa_action', true);
                $this->log(sprintf("Attribute %s added for:  %s ", $product_action, $product_id ));
                wp_set_object_terms( $product_id, $product_suitablefor, 'pa_suitablefor', true );
                $this->log(sprintf("Attribute %s added for:  %s ", $product_suitablefor, $product_id ));
                wp_set_object_terms( $product_id, $product_lactose_gluten, 'pa_lactose_gluten', true);
                $this->log(sprintf("Attribute %s added for:  %s ", $product_lactose_gluten, $product_id ));
                wp_set_object_terms( $product_id, $product_spf, 'pa_spf', true );
                $this->log(sprintf("Attribute %s added for:  %s ", $product_spf, $product_id ));
            }
        }
    }


    public function get_product_id($sku)
    {
        global $wpdb;
        $product_id = $wpdb->get_var(
            $wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_sku' AND meta_value LIKE '%%%s' ", $sku)
        );
        if ($product_id) {
            return $product_id;
        }
        return false;
    }

    
}
