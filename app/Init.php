<?php

namespace App;

use App\Base\Singleton;
use App\Base\Application;

use App\Theme\Theme;
use App\Theme\Brand;
use App\Theme\Header;
use App\Theme\WoocommerceImport;

use App\Theme\Language;
use App\Hooks\Pagination;
use App\Hooks\AllowSvgHook;
use App\Hooks\RegisterStrings;
use App\Hooks\SearchFilter;
use App\Hooks\WoocommerceHook;
use App\Base\Bootstrap\RegisterPostTypes;
use App\Base\Bootstrap\RegisterTaxonomies;
use App\Base\Bootstrap\RegisterAjaxHooks;


use App\Hooks\BodyHook;
use App\Hooks\HeadHook;
use App\Services\ProductImporter\Admin;
use App\Services\ProductImporter\Importer;
use App\Services\ProductImporter\UpdateAttributes;
use App\Services\ProductImporter\TranslateProducts;

class Init extends Singleton
{

    protected function __construct()
    {
        Application::init();
        Theme::init();
        Brand::init();
        Header::init(); 
        Pagination::init();
        Language::init();
        RegisterPostTypes::init();
        RegisterTaxonomies::init();
        RegisterAjaxHooks::init();
        /**
         * Start of hooks
         */
        HeadHook::init();
        BodyHook::init();
        AllowSvgHook::init();
        RegisterStrings::init();
        SearchFilter::init();
        WoocommerceHook::init();

        Admin::init();
        Importer::init();
        UpdateAttributes::init();
        TranslateProducts::init();
    }
}