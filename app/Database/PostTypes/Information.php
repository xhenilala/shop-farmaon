<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class Information
{
    public function create()
    {
        return PostType::create()
            ->slug('information')
            ->name(__('Information', 'farmaon'))
            ->menu_icon('dashicons-list-view')
            ->register();
    }
}