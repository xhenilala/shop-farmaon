<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class Service
{
    public function create()
    {
        return PostType::create()
            ->slug('service')
            ->name(__('Service', 'farmaon'))
            ->menu_icon('dashicons-editor-ol')
            ->register();
    }
}