<?php

namespace App\Base\Debug;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

class MonologLogger
{
    protected $logger;
    protected $loggerName;
    protected $path;

    public function __construct()
    {
        $this->loggerName = config('app.logs.logger_name', 'nmc');
        $this->logger = new Logger($this->loggerName);
        
        $this->path = __DIR__ . '/../../../logs/' . $this->loggerName .'.log';
        $this->logger->pushHandler(new RotatingFileHandler($this->path, 30, Logger::DEBUG));
        
    }

    public function get()
    {
        return $this->logger;
    }

    public function write($data)
    {
        if (!$this->logger) {
            return;
        }
        $this->logger->error($data['exception']['message'], (array) $data);
    }
}
