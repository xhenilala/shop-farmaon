<?php

namespace App\Hooks;

use App\Base\Singleton;

class WoocommerceHook extends Singleton
{
    protected function __construct()
    {
        //remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
        add_action('woocommerce_after_shop_loop_item_title', [ $this, 'add_product_short_description' ], 3);
        add_action('woocommerce_before_shop_loop_item_title', [ $this, 'image_overlay' ]);
        add_filter('woocommerce_breadcrumb_defaults', [ $this, 'change_breadcrumb_delimiter' ]);
        //add_action('woocommerce_before_shop_loop_item_title', [$this, 'add_product_thumbnail'], 10);
        // remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
        add_filter('woocommerce_checkout_fields', [ $this,  'custom_remove_woo_checkout_fields' ]);
        add_filter('woocommerce_checkout_fields', [ $this, 'override_checkout_city_fields']);

        add_filter('woocommerce_checkout_fields', [ $this, 'override_postcode_validation' ]);

        add_action('woocommerce_before_shop_loop_item_title', [ $this, 'sale_discount'], 25);

        add_filter('woocommerce_format_sale_price', [ $this, 'woocommerce_custom_sales_price'], 10, 3);
        // add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );

        add_filter('loop_shop_per_page', [ $this, 'productsPerPage' ], 20);

        // first purchase discount
        add_action('woocommerce_cart_calculate_fees', [ $this, 'set_discount']);
        
        //add_action('woocommerce_single_product_summary', 'woocommerce_product_additional_information_tab', 70, 0);
        
        add_filter('woocommerce_email_subject_customer_completed_order', [$this, 'change_customer_email_subject'], 1, 2);

        //do_action( 'woocommerce_email_header', [$this, 'change_email_heading_customer'], 10, 2);
        add_filter('woocommerce_email_heading_customer_completed_order', [ $this, 'change_email_title_header'], 10, 2);

        add_filter( 'woocommerce_product_related_posts_relate_by_category', [$this, 'related_products'], 10, 2);
        add_filter( 'woocommerce_product_related_posts_relate_by_tag', [$this, 'exclude_tags']);

        //add_action( 'woocommerce_proceed_to_checkout', [$this, 'move_apply_cupon'], 10 );
        remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
        // nuk eshp mire prap 2 min
    }

    public function coupon_applied($cart)
    {
        return in_array('giveaway15', $cart->get_applied_coupons());
    }
 
    public function move_apply_cupon() 
    {
    ?> 
        <form class="woocommerce-coupon-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
            <?php if ( wc_coupons_enabled() ) { ?>
                <div class="coupon under-proceed mt-3 mb-3">
                <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" style="width: 100%" /> 
                <button type="submit" class="btn btn-primary" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'farmaon' ); ?>" style="width: 100%"><?php esc_attr_e( 'Apply coupon', 'farmaon' ); ?></button>
                </div>
            <?php } ?>
        </form>
    <?php
    }

    public function change_email_title_header($email_heading, $order)
    {
        if ('bkt' == $order->get_payment_method()) {
            $email_heading = __('Faleminderit për pagesën!', 'farmaon');
        } elseif ('cod' == $order->get_payment_method()) {
            $email_heading = __('Faleminderit për porosinë!', 'farmaon');
        }
        return $email_heading;
    }
    

    public function change_customer_email_subject($subject, $order)
    {
        $payment_method = $order->get_payment_method();
        
        if ($payment_method == 'btk') {
            $subject = __('Faleminderit për pagesën!', 'farmaon');
        } elseif ($payment_method == 'cod') {
            $subject = __('Faleminderit për porosinë!', 'farmaon');
        }
        return $subject;
    }


    public function returning_customer($email)
    {
        global $wpdb;
        $has_ordered = false;
        $customer_id = $wpdb->get_var($wpdb->prepare("
            SELECT meta_value 
            FROM $wpdb->postmeta 
            WHERE meta_key = '_billing_email'
            AND meta_value = '%s'
            ", $email));

        if ($customer_id) {
            $has_ordered = true;
        }

        return $has_ordered;
    }

    
    public function set_discount($cart)
    {
    
        global $woocommerce;
        
        if (! $_POST || ( is_admin() && ! is_ajax() )) {
            return;
        }
        
        if (isset($_POST['post_data'])) {
            parse_str($_POST['post_data'], $post_data);
        } else {
            $post_data = $_POST;
        }
        // print_r($post_data);
        $email = @$post_data['billing_email'];
        if ($this->returning_customer($email) || $this->coupon_applied($cart)) {
            return;
        }
        
        $this->add_firstbye_discount();

    }
    

    public function add_firstbye_discount()
    {
        global $woocommerce;

        $total = $this->getTotal();
        $fee = - (10/100 * $total);
        $woocommerce->cart->add_fee(pll__('Discount first buy'), $fee, true, 'standard');
    }

    public function getTotal()
    {
        global $woocommerce;
        $total = 0;
        $cart_items = $woocommerce->cart->get_cart();

        foreach ($cart_items as $item) {
            $product = WC_get_product($item['data']->get_id());
            if (!$product->is_on_sale()) {
                $line_total = $item['line_total'];
                $total+= $line_total;
            }
        }
        return $total;
    }

    public function productsPerPage($cols)
    {
        $cols = 48;
        return $cols;
    }


    public function woocommerce_custom_sales_price($price, $regular_price, $sale_price)
    {
        if (is_admin()) {
            return $price;
        }

        if (!is_numeric($regular_price) || !is_numeric($sale_price)) {
            global $product;
            if ($product) {
                if ($product->get_children()) {
                    $regular_price = wc_get_price_to_display($product, array( 'price' => $product->get_variation_regular_price('max') ));
                } else {
                    $regular_price = wc_get_price_to_display($product, array( 'price' => $product->get_regular_price() ));
                }
                $regular_price = wc_get_price_to_display($product, array( 'price' => $product->get_variation_regular_price('max') ));
                $sale_price = wc_get_price_to_display($product);
            } else {
                return $price;
            }
        }
        

        $regular_price = (float) $regular_price;
        $sale_price = (float) $sale_price;
        
        $percent = (int) ((string) ((($regular_price - $sale_price) / $regular_price) * 100));
        $regular_price_html = '<del>'.wc_price($regular_price).'</del> ';
        $sale_price_html = '<ins>'.wc_price($sale_price).'</ins> ';
        $percent_html = '<span class="custom-price-percent"> -' . $percent . '%</span>';
        $regular_price_html = '<span class="custom-product-price-sale">' . $regular_price_html . '</span>';
        return $regular_price_html . $sale_price_html . $percent_html ;
    }
    
    public function formatPrice($price)
    {
        return filter_var(strip_tags($price), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION|FILTER_FLAG_ALLOW_THOUSAND) ;
    }
  
    public function sale_discount()
    {
        global $product;
        if (! $product->is_on_sale()) {
            return;
        }
        if ($product->is_type('simple')) {
            $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
        } elseif ($product->is_type('variable')) {
            $max_percentage = 0;
            foreach ($product->get_children() as $child_id) {
                $variation = wc_get_product($child_id);
                $price = $variation->get_regular_price();
                $sale = $variation->get_sale_price();
                if ($price != 0 && ! empty($sale)) {
                    $percentage = ( $price - $sale ) / $price * 100;
                }
                if ($percentage > $max_percentage) {
                    $max_percentage = $percentage;
                }
            }
        }
        if ($max_percentage > 0) {
            echo "<div class='sale-perc'>-" . round($max_percentage) . "%</div>";
        }
    }

    public function add_product_short_description()
    {
        global $product;
        echo '<span class="short-description">';
        $desc = the_excerpt();
        echo strip_tags($desc);
        echo '</span>';
    }

    public function image_overlay()
    {
        echo '<div class="product-card__overlay">
                <button class="buy-now-link">Më Shumë</button>
		    </div>';
    }
    
    public function change_breadcrumb_delimiter($defaults)
    {
        $defaults['delimiter'] = '<span class="delimiter"> > </span>';
        return $defaults;
    }
    
    public function add_product_thumbnail()
    {
        echo '<div class="woo-image">'.woocommerce_get_product_thumbnail('full').'</div>';
    }

    
 
    public function custom_remove_woo_checkout_fields($fields)
    {

        // remove billing fields
        unset($fields['billing']['billing_company']);
        unset($fields['billing']['billing_address_2']);
        unset($fields['billing']['billing_country']);
        unset($fields['billing']['billing_state']);
        // unset($fields['billing']['billing_postcode']);
    
        // remove shipping fields
        unset($fields['shipping']['shipping_company']);
        unset($fields['shipping']['shipping_address_2']);
        unset($fields['shipping']['shipping_country']);
        unset($fields['shipping']['shipping_state']);
        // unset($fields['shipping']['billing_postcode']);
        
        //unset billing fields label
        unset($fields['billing']['billing_first_name']['label']);
        unset($fields['billing']['billing_last_name']['label']);
        unset($fields['billing']['billing_address_1']['label']);
        unset($fields['billing']['billing_city']['label']);
        unset($fields['billing']['billing_phone']['label']);
        unset($fields['billing']['billing_email']['label']);
        unset($fields['billing']['billing_postcode']['label']);

        //set billing fields label
        $fields['billing']['billing_first_name']['placeholder'] = 'Emri *';
        $fields['billing']['billing_last_name']['placeholder'] = 'Mbiemri *';
        $fields['billing']['billing_address_1']['placeholder'] = 'Adresa *';
        $fields['billing']['billing_city']['placeholder'] = 'Qyteti *';
        $fields['billing']['billing_phone']['placeholder'] = 'Numri i Telefonit *';
        $fields['billing']['billing_email']['placeholder'] = 'Adresa e-mail *';
        $fields['billing']['billing_postcode']['placeholder'] = 'Kodi Postar ';

        //unset shipping fields label
        unset($fields['shipping']['shipping_first_name']['label']);
        unset($fields['shipping']['shipping_last_name']['label']);
        unset($fields['shipping']['shipping_address_1']['label']);
        unset($fields['shipping']['shipping_city']['label']);
        unset($fields['shipping']['shipping_phone']['label']);
        unset($fields['shipping']['shipping_email']['label']);
        unset($fields['shipping']['shipping_postcode']['label']);

        //set shipping fields label
        $fields['shipping']['shipping_first_name']['placeholder'] = 'Emri *';
        $fields['shipping']['shipping_last_name']['placeholder'] = 'Mbiemri *';
        $fields['shipping']['shipping_address_1']['placeholder'] = 'Adresa *';
        $fields['shipping']['shipping_city']['placeholder'] = 'Qyteti *';
        $fields['shipping']['shipping_phone']['placeholder'] = 'Numri i Telefonit *';
        $fields['shipping']['shipping_email']['placeholder'] = 'Adresa e-mail *';
        $fields['shipping']['shipping_postcode']['placeholder'] = 'Kodi Postar ';
        
        return $fields;
    }

    public function override_postcode_validation($fields)
    {
        $fields['billing']['billing_postcode']['required'] = false;
        $fields['shipping']['shipping_postcode']['required'] = false;
        return $fields;
    }
 
    
    public function override_checkout_city_fields($fields)
    {

        $option_cities = array(
                '' => __('Zgjidh qytetin'),
                'Kosove' => 'Kosove',
                'Tirane' => 'Tirane',
                'Durrës' => 'Durrës',
                'Elbasan' => 'Elbasan',
                'Vlore' => 'Vlore',
                'Shkodër' => 'Shkodër',
                'Korçë' => 'Korçë',
                'Fier' => 'Fier',
                'Berat' => 'Berat',
                'Lushnje' => 'Lushnje',
                'Pogradec' => 'Pogradec',
                'Kavaje' => 'Kavaje',
                'Laç' => 'Laç',
                'Lezhë' => 'Lezhë',
                'Kukës' => 'Kukës',
                'Gjirokastër' => 'Gjirokastër',
                'Patos' => 'Patos',
                'Krujë' => 'Krujë',
                'Kuçovë' => 'Kuçovë',
                'Sarandë' => 'Sarandë',
                'Peshkopi' => 'Peshkopi',
                'Burrel' => 'Burrel',
                'Cërrik' => 'Cërrik',
                'Çorovodë' => 'Çorovodë',
                'Shijak' => 'Shijak',
                'Librazhd' => 'Librazhd',
                'Tepelenë' => 'Tepelenë',
                'Gramsh' => 'Gramsh',
                'Poliçan' => 'Poliçan',
                'Bulqizë' => 'Bulqizë',
                'Përmet' => 'Përmet',
                'Fushë-Krujë' => 'Fushë-Krujë',
                'Kamzë' => 'Kamzë',
                'Rrëshen' => 'Rrëshen',
                'Ballsh' => 'Ballsh',
                'Mamurras' => 'Mamurras',
                'Bajram Curr' => 'Bajram Curr',
                'Ersekë' => 'Ersekë',
                'Peqin' => 'Peqin',
                'Divjakë' => 'Divjakë',
                'Selenicë' => 'Selenicë',
                'Bilisht' => 'Bilisht',
                'Roskovec' => 'Roskovec',
                'Këlcyrë' => 'Këlcyrë',
                'Pukë' => 'Pukë',
                'Memaliaj' => 'Memaliaj',
                'Rrogozhinë' => 'Rrogozhinë',
                'Ura Vajgurore' => 'Ura Vajgurore',
                'Himarë' => 'Himarë',
                'Delvinë' => 'Delvinë',
                'Vorë' => 'Vorë',
                'Koplik' => 'Koplik',
                'Maliq' => 'Maliq',
                'Përrenjas' => 'Përrenjas',
                'Shtërmen' => 'Shtërmen',
                'Krumë' => 'Krumë',
                'Libohovë' => 'Libohovë',
                'Orikum' => 'Orikum',
                'Fushë-Arrëz' => 'Fushë-Arrëz',
                'Shëngjin' => 'Shëngjin',
                'Rubik' => 'Rubik',
                'Milot' => 'Milot',
                'Leskovik' => 'Leskovik',
                'Konispol' => 'Konispol',
                'Krastë' => 'Krastë',
                'Kërrabë' => 'Kërrabë',
            );

        $fields['billing']['billing_city']['type'] = 'select';
        $fields['billing']['billing_city']['options'] = $option_cities;
        $fields['shipping']['shipping_city']['type'] = 'select';
        $fields['shipping']['shipping_city']['options'] = $option_cities;

        return $fields;
    }

    public function related_products($acivated, $product_id)
    {
        $terms = wp_get_post_terms($product_id, 'product_cat');
        foreach ($terms as $key => $term) {
            $children = get_categories(array('parent' => $term->term_id, 'taxonomy' => 'product_cat'));
            if (empty($children)) {
                $cats_array[] = $term->term_id;
            }
        }
        
        return $cats_array;
    }
    
    public function exclude_tags()
    {
        return [];
    }
    
}
