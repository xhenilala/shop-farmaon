<?php

namespace App\Hooks;

use App\Base\Singleton;


class HeadHook extends Singleton
{
    protected function __construct()
    {
        add_action( 'init',[ $this, 'clear_main_menu_transient' ]  );
        add_action('admin_bar_menu', [$this, 'add_toolbar_items' ] , 100);
        add_action('init', [$this, 'clear_main_menu_transient' ] , 100);
    }


        /**
        * add admin bar menu ( delete main menu transient )
        */

        function add_toolbar_items($admin_bar){
            $admin_bar->add_menu( array(
                'id'    => 'purge_main_menu_transients',
                'title' => 'Delete menu transient',
                'href'  => get_dashboard_url().'?_delete_transients',
                'meta'  => array(
                ),
            ));
        }

        function clear_main_menu_transient(){
            if( isset( $_GET['_delete_transients'] ) ){
                    global $wpdb; 
                    $delete = $wpdb->query("DELETE FROM $wpdb->options  WHERE option_name LIKE '%_transient_%' ");
            }
        }
}
