<?php

namespace App\Hooks;

use App\Base\Singleton;

class BodyHook extends Singleton
{
    protected function __construct()
    {
        add_filter('body_class', [$this, 'body_classes']);
        // change excerpt length
        add_filter( 'excerpt_length', [$this, 'custom_excerpt_length' ], 999 );

        add_filter( 'gettext', [$this,'force_translate'], 20, 3 );
        add_filter( 'woocommerce_checkout_fields', [$this,'md_custom_woocommerce_checkout_fields'] );

        // // change excerpt length
        // add_filter('excerpt_length', [$this, 'custom_excerpt_length' ]);
 
        // translate product categories
        add_filter('get_term', [$this, 'translate_term'], 10, 1);

        // translate post title
        add_filter('the_title', [ $this, 'set_post_title' ], 50, 2);

        // translate post content
        add_filter('the_content', [ $this, 'set_post_content'], 50);
        add_filter('the_excerpt', [ $this, 'set_post_excerpt'], 60);
        add_filter('pll_the_language_link', [$this, 'my_link'], 10, 2);

        // remove translation support for pages
        add_filter('pll_get_post_types', [ $this, 'pll_remove_post_type_support'], 50, 1);


        add_filter('pre_get_posts', [ $this, 'filter_by_lang' ], 10, 3);

    }

    /**
     * Adds custom classes to the array of body classes.
     *
     * @param array $classes Classes for the body element.
     * @return array
     */
    public function body_classes($classes)
    {
        return $classes;
    }

    public function custom_excerpt_length( $length )
    {
        // if ( is_admin() ) {
        //     return $length;
        // }
        return 10;
    }

    
    /**
     * Change comment form default field names.
     *
     * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
     */
    public function force_translate( $translated_text, $text, $domain ) {

        switch ( $translated_text ) {

            case 'View cart' :
                $translated_text = __( 'Shporta', 'farmaon' );
                break;

            case 'Search products' :
                $translated_text = __( 'Kërko produktet', 'farmaon' );
                break;

            case 'Checkout' :
                $translated_text = __( 'Kasa', 'farmaon' );
                break;

            case 'No products in the cart' :
            $translated_text = __( 'Nuk ka produkte në shportë', 'farmaon' );
                break;

            case 'Order notes (optional)' :
            $translated_text = __( 'Shënime rreth porosisë (opsioinale)', 'farmaon' );
                break;

            case 'Notes about your order, e.g special notes for delivery.' :
            $translated_text = __( 'Shënime për porosinë tuaj, p.sh. shënime të veçanta për dorëzim etj', 'farmaon' );
                break;

            case 'terms and conditions' :
            $translated_text = __( 'termat dhe kushtet e blerjes.', 'farmaon' );
                break;

            case 'Billing is a required field.' :
            $translated_text = __( 'Janë fusha të detyrueshme!', 'farmaon' );
                break;
            
            case 'Payment method:' :
            $translated_text = __( 'Metoda e pagesës:', 'farmaon' );
                break;
        }
        return $translated_text;
    }

    public function md_custom_woocommerce_checkout_fields( $fields ) 
    {
        $fields['order']['order_comments']['placeholder'] = 'Shënime për porosinë tuaj, p.sh. shënime të veçanta për dorëzim etj';
        $fields['order']['order_comments']['label'] = 'Shënime rreth porosisë (opsioinale)';

        return $fields;
    }


    public function translate_term($term)
    {
        if (get_locale()== 'en') {
            $name = get_field('post_title_en', $term);
            if ($name) {
                $term->name=$name;
            }
        }
        return $term;
    }

    public function post_type_list()
    {
        return [
            'page', 'product'
        ];
    }

    public function set_post_title($title, $id)
    {

        if (!$id) {
            return $title;
        }
        
        $post_type_list = $this->post_type_list();
        $post_type = get_post_type($id);
        $lang = get_locale();
        // var_dump($lang);

        if (!in_array($post_type, $post_type_list)) {
            return $title;
        }
        
        if ($lang == 'en' && get_field('post_title_en', $id)) {
            $title = get_field('post_title_en', $id);
        }
        // var_dump($title);
        return $title;
    }

    public function set_post_content($content)
    {
        if ((is_shop() || is_page() || is_single() ) && in_the_loop()) {
            global $post;
            $lang = get_locale();
            if ($lang == 'en' && get_field('post_content_en', $post->ID)) {
                $content = get_field('post_content_en', $post->ID);
            }
        }
        
        return $content;
    }

    public function set_post_excerpt($excerpt)
    {
        if ((is_shop() || is_page() || is_single() ) && in_the_loop()) {
            global $post;
            $lang = get_locale();
            if ($lang == 'en' && get_field('post_excerpt_en', $post->ID)) {
                $content = get_field('post_excerpt_en', $post->ID);
            }
        }
        
        return $excerpt;
    }
    
    public function term_link_filter($url, $term, $taxonomy)
    {
        return str_replace('product-category', 'kategoria-e-produktit', $url);
    }

    public function filter_by_lang($query)
    {
        if (( is_shop() || is_archive() || is_search() ) && $query->is_main_query()) {
            $lang = @$_COOKIE['pll_language'];
            if ($lang == 'en') {
                $query->set('lang', 'en');
            }
        }
        return $query;
    }
    
    
    public function my_link($url, $slug)
    {
        return $url === null ? home_url('?lang='.$slug) : $url;
    }

    public function pll_remove_post_type_support($post_types)
    {
        if (array_key_exists('page', $post_types)) {
            unset($post_types['page']);
        }

        return $post_types;
    }

}
