<?php

namespace App\Hooks;

use App\Base\Singleton;


class RegisterStrings extends Singleton
{
    protected function __construct()
    {
        add_action( 'widgets_init', [ $this, 'register_strings' ]);
    }

	public function register_strings(){
	    
	    $strings = [ 
			'Read More', 'Featured Products', 'View all products', 'New Arrival', 'Our Blog', 
			'Quick Links', 'Categories', 'Follow Us', 'Latest Articles', 'Order number:',
			'Date:', 'Total:', 'Payment method:', 'Price: ', 'SKU:', 
			'Category:', 'Tag:', 'Quantity', 'Billing details', 'Ship to a different address?', 
			'Your order',  'Place Order', 'Subtotal', 'Shipping', 'Login','View Cart', 'Checkout',
			'Remove', 'Cart totals', 'Proceed to checkout', 'Additional information', 'Create an account?', 'See More',
			'Return to shop', 'Your cart is currently empty.', 'Cart Totals', 'Total', 'Price', 'Product', 'Update cart', 
			'There are some issues with the items in your cart. Please go back to the cart page and resolve these issues before checking out.',
			'Return to cart', 'Billing &amp; Shipping', 'Billing details', 'Create an account?', 'Have a coupon?', 'Click here to enter your code',
			'If you have a coupon code, please apply it below.', 'Coupon code', 'Apply coupon', 'Returning customer?', 'Click here to login',
			'If you have shopped with us before, please enter your details below. If you are a new customer, please proceed to the Billing &amp; Shipping section.',
			'Qty', 'Totals', 'Search products …', '•  Powered by ', 'Latest products added', 'Read more', 'Related products', 'Categories:', 'Add to cart', 
			'Sorry, this product is unavailable. Please choose a different combination.', 'FarmaOn. All rights reserved.', 'Client service', 'Product security',
			'About us', 'Discount fist buy', 'Shipping fee', 'Just to let you know that your payment has been confirmed.', 'No products in the cart.'
	    ];

	    if( $strings )
	        foreach( $strings as $str )
	            pll_register_string( $str, $str, 'farmaon', false );

	}
}
