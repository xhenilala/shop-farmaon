<?php

namespace App\Hooks;

use App\Base\Singleton;


class SearchFilter extends Singleton
{
    protected function __construct()
    {
        add_action( 'pre_get_posts', [ $this, 'search_products_only'] );
    }

	

    function search_products_only( $query ) {
        if( ! is_admin() && is_search() && $query->is_main_query() ) {
            $query->set( 'post_type', 'product' );
        }
    }

}
