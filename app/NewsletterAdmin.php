<?php
namespace App;

if( !class_exists('WP_List_Table') ){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class NewsletterAdmin extends \WP_List_Table{

    function __construct(){
        
        global $status, $page;

        parent::__construct( array(
            'singular'  => 'contact',
            'plural'    => 'contacts',
            'ajax'      => false
        ) );
    }

    function column_default( $item, $column_name ){
        
        switch( $column_name ){
            case 'email_address':
            case 'ip_address':
            case 'u_agent':            
            case 'date_created':
                return $item[$column_name];
            default:
                return false;
        }
    }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'],
            $item['ID']
        );
    }

    function get_columns(){
        
        $columns = array(
            'cb'              => '<input type="checkbox" />',
            'email_address'   => __( 'Email Address' , 'farmaon' ),  
            'ip_address'      => __( 'IP Address' , 'farmaon' ),
            'u_agent'         => __( 'User Agent' , 'farmaon' ),            
            'date_created' => __( 'Date Registered' , 'farmaon' ),
        );

        return $columns;
    }

    function get_sortable_columns() {
        
        $sortable_columns = array(
            'email_address'     => array( 'email_address', false ),
            'ip_address'        => array( 'ip_address', false ),
            'u_agent'           => array( 'u_agent', false ),
            'date_created'   => array( 'date_created', false ),
        );

        return $sortable_columns;
    }

    function get_bulk_actions() {
        
        $actions = array(
            'delete'    => __( 'Remove' , 'farmaon' ),
            'export'    => __( 'Export CSV' , 'farmaon' ),
            'export_rcs' => __( 'Export RCS' , 'farmaon' ),
        );

        return $actions;
    }

    function process_bulk_action() {
       
        global $wpdb;
        $table_name = $wpdb->prefix . 'newsletter';
        $contacts   = @$_GET['contact'];

        # Delete user from DB
        if( 'delete' === $this->current_action() ){

            if ( is_array( $contacts ) )
                foreach ( $contacts as $contact )
                    $wpdb->delete( $table_name, array( 'ID' => $contact ) );
        }

        # Export contact list
        if ( 'export' === $this->current_action() || 'export_rcs' == $this->current_action() ) {
            
            if( 'export_rcs' === $this->current_action() ){
                if(isset($_GET['contact'])){
                    $i = 0;
                    $contacts = $_GET['contact'];
                    foreach( $contacts as $contact){
                         $row = $wpdb->get_results(
                            sprintf( 'select `email_address`, `ip_address`, `u_agent`, `date_created` from `%s` where ID = %s' , $table_name, $contact ), OBJECT
                        );
                        $contact_list[$i]['email_address']      = $row[0]->email_address;
                        $contact_list[$i]['ip_address']         = $row[0]->ip_address;
                        $contact_list[$i]['u_agent']            = $row[0]->u_agent;
                        $contact_list[$i]['date_created']       = $row[0]->date_created;

                        $i++;
                    }
                  
                }else{
                    $contact_list = $wpdb->get_results(
                        sprintf( 'select `email_address`, `ip_address`, `u_agent`, `date_created` from `%s`' , $table_name ), ARRAY_A
                    );
                }
            }else{
                if(isset($_GET['contact'])){
                    $i = 0;
                    $contacts = $_GET['contact'];
                    foreach( $contacts as $contact){
                         $row = $wpdb->get_results(
                            sprintf( 'select `email_address`, `ip_address`, `u_agent`, `date_created` from `%s` where ID = %s' , $table_name, $contact ), OBJECT
                        );
                        $contact_list[$i]['email_address']      = $row[0]->email_address;
                        $contact_list[$i]['ip_address']         = $row[0]->ip_address;
                        $contact_list[$i]['u_agent']            = $row[0]->u_agent;
                        $contact_list[$i]['date_created']       = $row[0]->date_created;
                        $i++;
                    }
                  
                }else{            
                    $contact_list = $wpdb->get_results(
                        sprintf( 'select `email_address`, `ip_address`, `u_agent`, `date_created` from `%s`' , $table_name ), ARRAY_A
                    );
                }
            }

            if ( !empty($contact_list) ) {
               
                $filename  = ( 'export_rcs' === $this->current_action() ) ? 'rcs_list_date_' : 'list_date_';
                $delimiter = ( 'export_rcs' === $this->current_action() ) ? '|' : ','; 
                $heading   = ( 'export_rcs' === $this->current_action() ) ?  
                    $heading = array(
                        __('Nr.'              , 'farmaon'),
                        __('Email Address'    ,'farmaon'),
                    )
                    : 
                    $heading = array(
                        __('Email Address'    ,'farmaon'),
                        __('Ip Address'       ,'farmaon'),
                        __('User Agent'       ,'farmaon'),
                        __('Registered Date'  ,'farmaon')
                    ); 

                $file_extension = '.csv';
                if( 'export_rcs' === $this->current_action() ){
                    $file_extension = '.txt';
                }

                $upload_dir         = wp_upload_dir();
                $csv_file           = $upload_dir['basedir'] . $filename . date('Y_m_d_') . time() . $file_extension;
                $csv_download_link  = $upload_dir['baseurl'] . $filename . date('Y_m_d_') . time() . $file_extension;
                
                if('export_rcs' === $this->current_action()){
                    $i = 1;
                    foreach ( $contact_list as $contact ){
                        array_unshift($contact, $i);
                        $parts = explode('@', $contact['email_address']);
                        // format |||0|X|||0|1
                        array_push($contact, $parts[0]);
                        array_push($contact, '');
                        array_push($contact, '0');
                        array_push($contact, 'X');
                        array_push($contact, '');
                        array_push($contact, '');
                        array_push($contact, '0');
                        array_push($contact, '1');
                        file_put_contents( $csv_file , implode($delimiter, $contact) . "\n", FILE_APPEND );
                        $i++;
                    }
                }else{
                    file_put_contents( $csv_file , implode($delimiter, $heading). "\n");
                    foreach ( $contact_list as $contact ){
                        file_put_contents( $csv_file , implode($delimiter, $contact) . "\n", FILE_APPEND );
                        $i++;
                    }
                }

                
                printf( '<a href="%s" style="float:left;margin-top:20px;padding:10px;background-color:green;color:#fff;text-decoration:none;text-transform:uppercase;" download>Download file</a>', $csv_download_link );
                
            }
        }

    }

    function prepare_items() {
 
        global $wpdb;
        $per_page   = 50;

        $table_name = $wpdb->prefix . 'newsletter';
        $hidden     = array();

        $columns    = $this->get_columns();
        $sortable   = $this->get_sortable_columns();

        $search_term    = ( !empty($_REQUEST['s'])) ? trim($_REQUEST['s']) : false ;
        $orderby        = ( !empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'ID';
        $order          = ( !empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc';
        
        $this->_column_headers = array( $columns, $hidden, $sortable );        
        $this->process_bulk_action();

        $total_coupons  = $wpdb->get_results( sprintf( 'select *, count(*) as cnt from `%s` where 1 = 1' , $table_name ) );

        // Get current page 
        $current_page   = $this->get_pagenum();
        $total_items    = @$total_coupons[0]->cnt;

        // Set query
        $main_query = sprintf( 'select * from %s where 1 = 1 ', $table_name );

        if ( $search_term )
            $main_query .= sprintf( ' and email_address = "%s" ', $search_term );

        $main_query .=  sprintf( ' order by %s %s limit %s,%s' , $orderby, $order, ( ($current_page-1) * $per_page ), $per_page );

        // Load items
        $this->items = $wpdb->get_results( $main_query , ARRAY_A );

        // Set paginations args
        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items/$per_page)
        ));
    }

}