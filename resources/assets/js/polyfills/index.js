
import './element.matches'
import './element.closest'
import './nodelist.foreach'
import './element.classList'

// if ('IntersectionObserver' in window &&
//     'IntersectionObserverEntry' in window &&
//     'intersectionRatio' in window.IntersectionObserverEntry.prototype) {
//   // Minimal polyfill for Edge 15's lack of `isIntersecting`
//   // See: https://github.com/w3c/IntersectionObserver/issues/211
//   if (!('isIntersecting' in window.IntersectionObserverEntry.prototype)) {
//     Object.defineProperty(window.IntersectionObserverEntry.prototype,
//       'isIntersecting', {
//       get: function () {
//         return this.intersectionRatio > 0;
//       }
//     });
//   }
// } else {
//   import('intersection-observer' /* webpackChunkName: "polyfills/intersection-observer" */)
// }
