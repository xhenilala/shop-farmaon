class MobileNavigation {
  constructor(container = null) {
    this.container = container
    console.log(this.container);
    this.menuLinks = container.querySelectorAll('cat-item a')
    this.onMenuPress = this.onMenuPress.bind(this)
    this.onMenuDoublePress = this.onMenuDoublePress.bind(this)
    this.addEventListeners()
  }

  addEventListeners() {
    this.container.addEventListener('click', this.onMenuPress)
    this.container.addEventListener('dblclick', this.onMenuDoublePress);
  }

  /**
   * 
   * @param {Event} event 
   */
  onMenuPress(event) {
    const clickedLink = event.target.closest('.js-mobile-navigation-link');
    if(!clickedLink) { return }
    event.preventDefault();

    if(event.detail && event.detail > 1) {
      return
    }
    clickedLink.parentNode.classList.toggle('show')
  }

  /**
   * 
   * @param {Event} event 
   */
  onMenuDoublePress(event) {
    event.preventDefault()
    console.log(event)
    const clickedLink = event.target.closest('.js-mobile-navigation-link');
    if(!clickedLink) { return }
    event.preventDefault();
    const href = clickedLink.getAttribute('href')
    window.location = href;
  }
}

export default MobileNavigation