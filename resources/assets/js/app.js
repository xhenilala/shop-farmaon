/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';

window.BRAKE_POINTS = {
  xs: 0,
  sm: 576,
  md: 900,
  lg: 1200,
  xl: 1600
}
import { wp_ajax } from './wp'
import Sliders from './Sliders'
import { Translate } from './wp';
import MobileNavigation from './MobileNavigation'
import Drawer from './Drawer'


const sliders = window.SLIDERS = new Sliders;



(function () {
  Array.from(document.querySelectorAll('.js-mobile-navigation')).forEach(container => {
    new MobileNavigation(container);
  })
  new Drawer({
    containerSelector: '.js-mobile-main-menu',
    buttonSelector: '.js-toggle-mobile-menu', 
    overlaySelector: '.js-mobile-main-menu-overlay',
  })
  sliders.add('.js-main-carousel', {
    setGallerySize: true,
    // adaptiveHeight: true,
    wrapAround: true,
  })
  wp_ajax('dummy_ajax2').get()
    .then(response => console.log(response))
    .catch(error => console.log(error))

  console.log(Translate.get('test'))


//Google Map
function initMap()
{
  const location = document.getElementById('farma_map');
    if(!location) return;
    let coordinates = JSON.parse(location.dataset.marker);
    console.log(coordinates);
    coordinates = new google.maps.LatLng(coordinates.lat, coordinates.lng);
    
    const map = new google.maps.Map(location, {
        center: coordinates,
        zoom: 18,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });

    var marker = new google.maps.Marker({
      position: coordinates,
      map: map,
      //icon     : map_marker
    });
}

google.maps.event.addDomListener(window, 'load', initMap);


})();


$(document).ready(function () {
  $('.color-code').click(function(){
      $('#pa_color').val($(this).attr('data-attribute'));
      $('#pa_color').trigger('change');
  });
});

$('#billing_email').blur(function(){
  console.log(1);
  $(document.body).trigger("update_checkout");
});

  /*  Set label as placeholder value
  /*----------------------------------------------------*/
  // function set_placeholder($class) {
  //   $($class).each(function(index, value){
  //     var label = $(this).find('label').text();
  //     $(this).find('input').attr('placeholder', label);
  //     $(this).find('textarea').attr('placeholder', label);
  //     $(this).find('label').remove();
  //   });
  // };
  // set_placeholder('.form-row');