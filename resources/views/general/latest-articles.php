<?php  $query_args = [
            'post_type' => 'post', 
            'posts_per_page' => 2,
            'orderby' => 'date',
            'order' => 'DESC',
        ];
        $query = new WP_Query( $query_args );
    if( $query->have_posts() ) : ?>
        <?php while( $query->have_posts() ) : $query->the_post(); ?>
            <div class="col-12 col-lg-6">
                <?php view('general.article-card'); ?>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>