<div class="article-card">
    <div class="article-card__container">
        <a href="<?php the_permalink(); ?>">
        <div class="article-card__image" style="background-image: url(<?php the_post_img_url(); ?>);">
            <!-- <img src="<?php the_post_img_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid"> -->
        </div>
        </a>
        <div class="article-card__content">
            <a href="<?php the_permalink(); ?>">
                <div class="article-card__title">
                    <h5><?php the_title(); ?></h5>
                </div>
                <div class="article-card__date">
                    <p><?php echo get_the_date(); ?></p>
                </div>
                <div class="article-card__entry">
                    <p><?php echo wp_trim_words(get_the_content(),'25','...'); ?></p>
                </div>
            </a>
            <div class="article-card__link">
                <a href="<?php the_permalink(); ?>"><?php pll_e('Read more'); ?></a>
            </div>
        </div>
    </div>
</div>