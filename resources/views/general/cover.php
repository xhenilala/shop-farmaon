<?php 
if( is_page() ){
	global $post;
	$cover = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
	
}

if(is_singular( [ 'post' ] ) ){
	global $post;
	$cover = get_field('image');
	//$cover = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
}

?>
<div class="cover" style="background-image: url('<?php echo $cover; ?>');">

</div>