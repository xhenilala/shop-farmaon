<?php
    $current_language = get_locale();
    $language = pll_current_language();
    $entry_text_sq = get_field('entry_text');
    $entry_text_en = get_field('entry_text_en');
?>
<div class="home-section py-5">
    <div class="home-section__content">
    <?php while (have_posts()) :
        the_post(); ?>
        <h1><?php the_title(); ?></h1>

        <?php 
        echo $current_language;
        if ($current_language == 'sq') : ?>
            <p><?php echo $entry_text_sq; ?></p>
        <?php elseif ($current_language == 'en') : ?>
            <p><?php echo $entry_text_en; ?></p>
        <?php endif; ?>
    <?php endwhile; ?>
    </div>
</div>
