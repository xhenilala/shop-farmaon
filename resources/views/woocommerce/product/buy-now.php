<?php
    global $product;
    $link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );
    
    $label = __('Buy Now', 'farmaon');
?>
<div class="product-card__buynow">
    <div class="product-card__buynow__container">
        <object>
            <a class="btn btn-primary" title="<?php the_title(); ?>-<?php echo $label; ?>" href="<?php echo esc_url( $link ); ?>">
                <?php echo $label; ?>
            </a>
        </object>
    </div>
</div>