<?php $specifics = collect(get_field('specifics'));  ?>

<?php foreach($specifics as $s):?>
    <div class="additional-details">
        <h6><?php echo $s['title']; ?></h6>
        <?php echo $s['description']; ?>
    </div>
<?php endforeach; ?>