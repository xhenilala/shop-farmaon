<div class="product-card">
    <div class="product-card__container">

        <div class="product-card__top">
            <?php view('woocommerce.product.thumbnail'); ?>

            <?php view('woocommerce.product.buy-now'); ?>
        </div>

        <?php view('woocommerce.product.title'); ?>

        <?php view('woocommerce.product.description'); ?>

        <?php view('woocommerce.product.price'); ?>

        
        
    </div>
</div>