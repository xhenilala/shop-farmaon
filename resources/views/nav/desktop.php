<?php
$menu_items = \App\MainMenu::init()->get_tree();
?>

<div class="main-navigation">
    <div class="main-navigation__container">
        <ul class="nav main-navigation__nav top-level">
            <?php foreach ($menu_items as $key => $m) :
                $item = collect($m);
                $link_class = is_category_selected($m) ? ' is-selected': ''; ?>
            <li
                class="nav-item main-navigation__item <?php echo $item->get('children') ? 'has-children' : ''; ?>"
                itemscope="itemscope"
                itemtype="https://www.schema.org/SiteNavigationElement"
            >
                <a
                    title="<?php echo $item->get('name') ?>"
                    class="nav-link  main-navigation__link<?php echo $link_class; ?>"
                    href="<?php echo $item->get('link') ?>"
                >
                    <span><?php echo $item->get('name') ?></span>
                </a>
                <?php if ($item->get('children')) : ?>
                <div class="main-navigation__dropdown">
                    <div class="main-navigation__dropdown__container d-flex flex-column shadow">
                        <div class="main-navigation__dropdown__content">
                            <div class="main-navigation__dropdown__nav-container ">
                                <?php
                                view('nav.desktop-menu-level', [
                                    'items' => $item->get('children'),
                                    'level' => $item->get('level')
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<!-- <div class="languages">
    < ?php wp_nav_menu(array( 'theme_location' => 'primary'  )); ?>
</div> -->
