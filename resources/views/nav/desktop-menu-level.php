<?php

$level = $level ?? 0;

if (!isset($items) || !$items) {
    return;
}
$class = $level > 0 ? 'main-navigation__vertical-nav flex-column' : 'main-navigation__horizontal-nav';
$link_class = $level > 0 ? 'main-navigation__vertical-nav__link' : 'main-navigation__horizontal-nav__link';
$link_item_class = $level > 0 ? 'main-navigation__vertical-nav__item' : 'main-navigation__horizontal-nav__item';
?>
            
<?php if ($level === 0) : ?>
    <?php foreach (collect($items)->chunk(12) as $chunk) : ?>
        <ul class=" nav level-<?php echo $level + 1; ?> <?php echo $class; ?>">
            <?php foreach ($chunk as $ch) :
                $item = collect($ch); ?>
            <li
                class="nav-item <?php echo $link_item_class; ?> <?php echo $item->get('children') ? 'has-children' : ''; ?>"
                itemscope="itemscope"
                itemtype="https://www.schema.org/SiteNavigationElement"
            >
                <a
                    title="<?php echo $item->get('name') ?>"
                    class="nav-link <?php echo $link_class; ?>"
                    href="<?php echo $item->get('link') ?>"
                >
                    <span><?php echo $item->get('name') ?></span>
                </a>
                <?php
                view('nav.desktop-menu-level', [
                    'items' => $item->get('children'),
                    'level' => $item->get('level')
                ]);
                ?>
            </li>
            <?php endforeach; ?>
        </ul>
    <?php endforeach; ?>
<?php else : ?>
<ul class="nav level-<?php echo $level + 1; ?> <?php echo $class; ?>">
    <?php foreach ($items as $ch) :
        $item = collect($ch); ?>
    <li
        class="nav-item <?php echo $link_item_class; ?> <?php echo $item->get('children') ? 'has-children' : ''; ?>"
        itemscope="itemscope"
        itemtype="https://www.schema.org/SiteNavigationElement"
    >
        <a title="<?php echo $item->get('name') ?>" class="nav-link <?php echo $link_class; ?>" href="<?php echo $item->get('link') ?>">
            <span><?php echo $item->get('name') ?></span>
        </a>
        <?php
        view('nav.desktop-menu-level', [
            'items' => $item->get('children'),
            'level' => $item->get('level')
        ]);
        ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
