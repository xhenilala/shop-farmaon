<div class="featured-section">
    <div class="container py-5">
        <div class="row">
            <div class="col">
                <div class="home-section">
                    <div class="home-section__content">
                        <h1><a href="./product-category/oferte/"><?php pll_e('Featured Products'); ?></a></h1>
                        <?php $content = get_field('featured_products_'.pll_current_language(),'options'); echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-4">
            <?php  $query_args = [
                    'post_status'       => 'publish',
                    'post_type'         => 'product',
                    'meta_query'        => WC()->query->get_meta_query(),
                    'post__in'          => array_merge( array( 0 ), wc_get_product_ids_on_sale() ),
                    'posts_per_page'    => 10,
                ];
                $query = new WP_Query( $query_args );
                if( $query->have_posts() ) : ?>
                <div class="col">
                    <div class='featured-products-carousel' data-flickity='{"groupCells": true, "cellAlign": "center" , "autoPlay": true, "wrapAround": true}'>
                        <?php while( $query->have_posts() ) : $query->the_post(); 
                            $_product = wc_get_product( get_the_ID() ); ?>
                            <div class="cell">
                                <div class="product-card">
                                    <div class="product-card__container">
                                        <div class="product-card___top">
                                            <div class="product-card__image">
                                                <?php echo $_product->get_image('medium'); ?>
                                                <div class="product-card__overlay">
                                                    <a href="<?php the_permalink(); ?>" class="buy-now-link"><?php echo pll_e('See More'); ?></a>
                                                </div>
                                            </div>
                                            <?php view('woocommerce.product.buy-now');?>
                                        </div>
                                            <div class="product-card__title">
                                                <h6><?php echo $_product->get_title();?></h6>
                                            </div>
                                            <div class="product-card__description">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                            <div class="product-card__price">
                                                <?php echo $_product->get_price_html(); ?>
                                            </div>
                                        
                                    </div>
                                </div>
                            </div>
                        
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
        </div>
    </div>
</div>