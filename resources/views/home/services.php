<?php
    $query = new WP_Query([
        'post_type' => 'service',
        'posts_per_page' => -1,
    ]);
    if( $query->have_posts() ) :  ?>
    <div class="container py-5">
        <div class="row">
            <?php while( $query->have_posts() ) : $query->the_post(); ?>
                <div class="col-12 col-md-4">
                    <div class="service py-4">
                        <div class="service__image pr-4">
                            <img src="<?php echo the_post_img_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid">
                        </div>
                        <div class="service__content">
                            <h5><?php the_title(); ?></h5>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>