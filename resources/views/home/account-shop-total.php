<span class="js-fragment-user-info">
    <?php if (get_current_user_id()) : ?>
    <span class="pn-dropdown-trigger" href="#" role="button">
        <a href="<?php echo home_url('/my-account/'); ?>" class="nav-link">
            <i class="fas fa-user"></i>
            
        </a>
        <div class="pn-dropdown">
            <div class="pn-dropdown__container">
                <div class="pn-dropdown__content p-0 account-dropdown-desktop">
                    <?php do_action('woocommerce_account_navigation'); ?>
                </div>
            </div>
        </div>
    </span>
    <?php else : ?>
    <span class="pn-dropdown-trigger" href="#" role="button">
        <a href="<?php echo home_url('/my-account/'); ?>" class="nav-link">
            <i class="fas fa-user"></i>
        </a>
    </span>
    <?php endif; ?>
</span>
<!-- <a href=""></a> -->
<div class="pn-dropdown-trigger" role="button">
    <span class="header-btn__icon header-minicart-icon">
        <a href="<?php echo home_url('/cart/'); ?>"><i class="fas fa-shopping-bag"></i></a>
        <span class="cart-items"><?php echo  WC()->cart->get_cart_contents_count(); ?></span>
    </span>
    <?php //view('header.shop', 'total-btn'); ?>
    <div class="pn-dropdown pn-dropdown--right">
        <div class="pn-dropdown__container">
            
            <div class="pn-dropdown__content">
                <h6 class="mb-0">
                    <?php pll_e('Produktet e fundit të shtuar' ); ?>
                </h6>
                <div class="widget_shopping_cart_content"><?php woocommerce_mini_cart(); ?></div>
            </div>
        </div>
    </div>
</div>
<div class="pn-dropdown-trigger ml-4" role="button">
    <span class="header-btn__icon header-minicart-icon">
        <a href="<?php echo home_url('/wishlist/'); ?>">
            <i class="fas fa-heart"></i>
        </a>
    </span>
</div>


