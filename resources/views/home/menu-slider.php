<?php
    $query = new WP_Query([
        'post_type' => 'slider',
        'posts_per_page' => -1,
    ]);
    if ($query->have_posts()) : ?>
<div class="container pt-4">
    <div class="row">
        <div class="col">
            <div class="js-main-carousel main-carousel">
            <?php while ($query->have_posts()) :
                $query->the_post();
                $btn = get_field('slider_btn');
                $mobile_img = get_field('mobile_image');
                $title = get_the_title(); ?>
                    <div class="carousel-cell main-carousel__cell">
                        <div class="main-carousel__cell-container">
                            <div class="main-carousel__mobile-image " style="background-image: url('<?php echo $mobile_img; ?>');" ></div>
                            <a class="main-carousel__link d-block" href="<?php echo $btn; ?>" >
                                <div class="main-carousel__desktop-image " style="background-image: url('<?php echo the_post_img_url(); ?>');" ></div>
                            </a>
                        </div>
                        <?php if ($title) : ?>
                        <div class="main-carousel__content">
                            <h4><?php the_title(); ?></h4>
                            <p><?php echo get_the_content(); ?></p>
                            <?php if ($btn) : ?>
                                <a href="<?php echo $btn; ?>" class="btn btn-primary--white mb-2"><?php pll_e('Read More'); ?></a>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                        <div class="main-carousel__overlay"></div>
                    </div>
            <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>
        <?php endif; ?>
