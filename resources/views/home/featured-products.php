<div class="container py-5">
    <div class="row">
        <div class="col">
            <div class="home-section">
                <div class="home-section__content">
                    <h1><?php echo pll_e('Featured Products'); ?></h1>
                    <?php $content = get_field('featured_products_'.pll_current_language(),'options'); echo $content; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-4">
        <!-- <div class="col"> -->
        <?php  $query_args = [
                    'post_type' => 'product',
                    'tax_query' => [
                        [
                            'taxonomy' => 'product_visibility',
                            'field'    => 'name',
                            'terms'    => 'featured',
                            'operator' => 'IN',
                        ],
                    ],
                    'posts_per_page' => 4,
                ];
                $query = new WP_Query( $query_args );
                if( $query->have_posts() ) : ?>
                    <?php while( $query->have_posts() ) : $query->the_post(); 
                    $_product = wc_get_product(  get_the_ID() ); ?>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="product-card">
                            <div class="product-card__container">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="product-card__image" >
                                        <?php echo $_product->get_image('medium'); ?>
                                    </div>
                                    <div class="product-card__overlay">
                                        <a href="<?php the_permalink(); ?>"><?php echo pll_e('Buy Now'); ?></a>
                                    </div>
                                </a>
                                    <div class="product-card__title">
                                        <h6><?php echo $_product->get_title();?></h6>
                                    </div>
                                    <div class="product-card__description">
                                        <p><?php echo get_the_excerpt(get_the_ID()); ?></p>
                                    </div>
                                    <div class="product-card__price">
                                        <?php echo $_product->get_price_html(); ?>
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                     <?php endwhile; ?>
                <?php endif; ?>
        <!-- </div> -->
    </div>
</div>