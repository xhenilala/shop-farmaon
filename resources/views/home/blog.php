<div class="container py-5">
    <div class="row">
        <div class="col">
            <div class="home-section">
                <div class="home-section__content">
                    <h1><a href="./blog"><?php echo pll_e('Our Blog'); ?></a></h1>
                    <?php $content = get_field('blog_'.pll_current_language(),'options'); echo $content; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-4">
        <?php view('general.latest-articles'); ?>
    </div>
</div>