<?php   $content = get_field('banner_'.pll_current_language(),'options'); 
        $button = get_field('banner_button','options');   ?>

<div class="container py-5">
    <div class="row">
        <div class="col">
            <div class="banner" style="background-image: url(<?php echo get_field('banner_image','options'); ?>);">
                <div class="banner__content">
                    <?php echo $content; ?>
                    <a href="<?php echo get_field('banner_button','options'); ?>" class="btn btn-primary"><?php echo pll_e('View all products'); ?></a>
                </div> 
            </div>
        </div>
    </div>
</div>