<div class="container py-5">
    <div class="row">
        <div class="col">
            <div class="home-section">
                <div class="home-section__content">
                    <h1><?php echo pll_e('New Arrival'); ?></h1>
                    <?php $content = get_field('new_products_'.pll_current_language(),'options'); echo $content; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-5">
        <?php  $query_args = [
                    'post_type'=> 'product',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'posts_per_page' => 6,
                ];
        $query = new WP_Query( $query_args );
        if( $query->have_posts() ) : ?>
        <div class="col">
            <div data-flickity='{ "groupCells": 3, "cellAlign": "center" , "autoPlay": true, "wrapAround": true}'>
                <?php while( $query->have_posts() ) : $query->the_post(); 
                $_product = wc_get_product(  get_the_ID() ); ?>
                <div class="cell">
                    <div class="product-card">
                        <div class="product-card__container">
                            <a href="<?php the_permalink(); ?>">
                                <div class="product-card__image">
                                    <?php echo $_product->get_image('medium'); ?>
                                    <div class="product-card__overlay">
                                        <a href="<?php the_permalink(); ?>" class="buy-now-link"><?php echo pll_e('See More'); ?></a>
                                    </div>
                                </div>
                                
                            </a>
                            <div class="product-card__title">
                                <h6><?php echo $_product->get_title();?></h6>
                            </div>
                            <div class="product-card__description">
                                <p><?php echo the_excerpt(); ?></p>
                            </div>
                            <div class="product-card__price">
                                <?php echo $_product->get_price_html(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        
        <?php endif; ?>
    </div>
</div>