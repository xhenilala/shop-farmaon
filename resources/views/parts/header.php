<div class="main-header py-4 d-none d-lg-block">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 d-flex align-items-center">
                <div class="social-items">
                    <?php view('general.social'); ?>
                </div>
            </div>
            <div class="col-12 col-md-4 d-flex align-items-center">
                <a class="site-logo m-auto" href="<?php echo esc_url(home_url('/')); ?>" >
                    <?php svg_logo([ 'style' => 'height: 52px; width: auto;' ]); ?>
                </a>
            </div>
            <div class="col-12 col-md-4 search-account d-flex align-items-center justify-content-end">
                <?php get_sidebar('search'); ?>
                <!-- <form class="site-search-form">
                   <label class="input-group expandSearch">
                       <input type="text" class="form-control site-search-form__input"  placeholder="<?php //spll_e('Search products …'); ?>" aria-label="Search …" value="" name="s">
                        
                   </label>
                </form> -->
                <?php view('home.account-shop-total'); ?>
            </div>
        </div>
        
    </div>
</div> 
<div class="farmaon-border d-none d-lg-block">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php view('nav.desktop'); ?>
            </div>
        </div>
    </div>
</div>