<?php
    $menu_items = [
        [
            'label' => 'Shop',
            'id' => 'nav-shop-tab',
            'icon_class' => 'fas fa-shopping-bag',
            'href' => '#nav-shop',
            'active' => true,
            'content_id' => 'nav-shop',
            'key' => 'shop_tab',
            'content' => get_mobile_menu_html(),
        ],
    
    ];
?>
<div class="js-mobile-main-menu drawer">
    <div class="js-mobile-main-menu-overlay drawer__overlay"></div>
    <div class="drawer__container">
        <div class="drawer__content tab-content" id="nav-tabContent">
            <?php foreach($menu_items as $m): $menu = collect($m); ?>
            <div
                class="tab-pane fade <?php echo $menu->get('active', false) ? 'show active' : '' ?>"
                id="<?php echo $menu->get('content_id'); ?>"
                role="tabpanel"
                aria-labelledby="<?php echo $menu->get('id'); ?>"
            >
                <?php echo $menu->get('content') ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>