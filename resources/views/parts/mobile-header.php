
<div class="mobile-header d-block d-sm-block d-md-block d-lg-none  py-3"> 
    <div class="container">
        <div class="row">
            <div class="col-12 mobile-header__items d-flex align-items-center justify-content-between">
                <a class="js-toggle-mobile-menu" role="button" class="js-toggle-mobile-menu btn btn--flat" href="#" data-tab="#nav-shop-tab">    
                    <i class="fas fa-bars"></i>
                </a>
                <a class="site-logo m-auto" href="<?php echo esc_url(home_url('/')); ?>" >
                    <?php svg_logo([ 'style' => 'height: 52px; width: 120px;' ]); ?>
                </a>
                <button type="button" class="search-button-mobile" data-toggle="modal" data-target="#exampleModal" ><i class="fas fa-search"></i></button>
                <a href="<?php echo home_url('/my-account/'); ?>"><i class="fas fa-user"></i></a>
                <a href="<?php echo home_url('/wishlist/'); ?>"><i class="fas fa-heart"></i></a>
                <a href="<?php echo home_url('/cart/'); ?>"><i class="fas fa-shopping-bag"></i></a>
                <span class="cart-items"><?php echo  WC()->cart->get_cart_contents_count(); ?></span>
            </div>
        </div>
    </div>
    <?php view('parts.categories'); ?>
</div>

<!-- search modal -->
<div class="container">
    <div class="row">
        <div class="col">
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header text-right">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="site-search-form">
                            <label class="sr-only" for="site-search-input"><?php echo _x('Kërkoni për:', 'label'); ?></label>
                            <div class="input-group">
                                <input
                                    type="text"
                                    class="form-control site-search-form__input"
                                    id="site-search-input"
                                    placeholder="<?php echo esc_attr_x('Kërkoni &hellip;', 'placeholder'); ?>"
                                    aria-label="<?php echo esc_attr_x('Kërkoni &hellip;', 'placeholder'); ?>"
                                    value="<?php echo get_search_query(); ?>"
                                    name="s"
                                >
                                <input 
                                    type="hidden" 
                                    name="post_type" 
                                    value="product"
                                >
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary site-search-form__button">
                                        <?php pll_e('Kerko'); ?>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>