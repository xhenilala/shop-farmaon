<div class="contact d-none d-lg-block">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="contact__item py-2 py-sm-3 d-flex align-items-center">
                    <span class="d-flex align-items-center pr-3">
                        <i class="fas fa-envelope"></i>
                        <p class="m-0 pl-3"><?php echo get_field('email_address','options'); ?></p>
                    </span>
                    <span class="d-flex align-items-center pl-3 border-left">
                        <i class="fas fa-phone"></i>
                        <p class="m-0 pl-3"><?php echo get_field('phone_number','options'); ?></p>
                    </span>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="contact__item py-2 py-sm-3 d-flex align-items-center justify-content-md-end">
                    <i class="fas fa-map-marker"></i>
                    <p class="m-0 pl-3"><?php echo get_field('address','options'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>