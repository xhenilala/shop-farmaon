<footer class="site-footer py-5">
    <div class="container px-md-5">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3 py-3 footer-description">
                <a href="<?php echo esc_url(home_url('/')); ?>" class="footer-logo site-logo">
                    <?php svg_logo([ 'style' => 'height: 52px; width: 120px;' ]); ?>
                </a>
                <div class="footer-description__content">
                    <?php $desc = get_field('company_description_'.pll_current_language(),'options'); echo $desc; ?>
                </div>
                <div class="footer-description__payment">
                    <?php $payment = get_field('payment','options'); 
                        if( $payment ): ?>
                        <img src="<?php echo $payment; ?>" alt="farma on" class="img-fluid payment-img pl-0">
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 py-3">
                <div class="footer-categories">
                    <h5><?php pll_e('Categories'); ?></h5>
                    <?php  wp_list_categories( [ 
                                                'taxonomy' 	 => 'product_cat',
                                                'hide_empty' => 1,
                                                'title_li' 	 => '',
                                                'hierarcial' => 1,
                                                'depth' => 1,
                                                ] ); ?>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-2 py-3 ">
                <div class="quick-links">
                    <h5 class="text-left"><?php pll_e('Client service'); ?></h5>
                    <div class="footer-links text-left">
                        <?php wp_nav_menu(array(    'theme_location' => 'service',
                                'container' => 'div',
                                'container_class' => 'footer-menu' )); ?>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-2 py-3 ">
                <div class="quick-links">
                    <h5 class="text-left"><?php pll_e('Product security'); ?></h5>
                    <div class="footer-links text-left">
                        <?php wp_nav_menu(array(    'theme_location' => 'security',
                                'container' => 'div',
                                'container_class' => 'footer-menu' )); ?>
                    </div>
                    
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-2 py-3 ">
                <div class="quick-links">
                    <h5 class="text-left"><?php pll_e('About us'); ?></h5>
                    <div class="footer-links text-left">
                        <?php wp_nav_menu(array(    'theme_location' => 'about-us',
                                'container' => 'div',
                                'container_class' => 'footer-menu' )); ?>
                    </div>
                    <h5 class="text-left  mt-md-2"><?php pll_e('Follow Us'); ?></h5>
                    <div class="footer-social mt-3">
                        <?php view('general.social'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-12 col-lg-6 py-2">
                <p class="m-0">&copy; <?php date('Y') ?> <?php pll_e('FarmaOn. All rights reserved.'); ?></p>
            </div>
            <div class="col-12 col-lg-6 py-2 d-flex justify-content-lg-end">
                <p class="m-0"><?php pll_e('•  Powered by '); ?><a class="text-light" href="http://farmacidaja.com/"><?php pll_e(' Farmaci Daja'); ?></a></p>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
<?php // view('parts.photoswipe') ?>