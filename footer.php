<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NMC_Theme
 */

?>

	</div><!-- #content -->
	<?php  view("parts.footer");  ?>
	<!-- < ?php if(isset($_GET['categories_export'])): ?>
	< ?php wp_list_categories(
		['taxonomy' 	 => 'product_cat','hide_empty' => 0,'title_li' 	 => '','hierarcial' => 1,]
	); ?>

	< ?php endif;?> -->
	 
</div><!-- #page --> 
<?php wp_footer(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144306646-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-144306646-1');
</script>
</body>
</html>
