<?php
    /* Template Name: Terms*/
    get_header();
?>
    <div class="terms">
        <div class="container py-5">
            <div class="row py-5">
                <div class="col">
                    <?php view('general.page-title');?>
                </div>
            </div>
            <div class="row pt-4">
                <?php while(have_posts()): the_post(); ?>
                    <div class="col-12">
                        <div class="terms__content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>