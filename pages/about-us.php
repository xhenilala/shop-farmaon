<?php
    /* Template Name: About us*/
    get_header();
?>
<div class="about-us">
    <div class="container">
        <div class="row pt-3">
            <div class="col">
                <?php woocommerce_breadcrumb(); ?>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col">
                <?php view('general.page-title'); ?>
            </div>
        </div>
        <div class="row py-5">
            <?php while(have_posts()): the_post(); ?>
            <div class="col-12 col-lg-5">
                <div class="about-us__image">
                    <img src="<?php echo get_field('image'); ?>" alt="<?php the_title(); ?>" class="img-fluid">
                </div>
            </div>
            <div class="col-12 col-lg-7">
                <div class="about-us__content">
                    <?php the_content(); ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>