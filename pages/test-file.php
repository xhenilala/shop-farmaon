<?php
    /* Template Name: Categories*/
    get_header();
?>
    <div class="terms">
        <div class="container py-5">
            <div class="row py-5">
                <div class="col">
                    <?php view('general.page-title');?>
                </div>
            </div>
            <div class="row pt-4">
                <?php while(have_posts()): the_post(); ?>
                    <div class="col-12">
                        <div class="terms__content">
                        <?php  wp_list_categories( ['taxonomy' 	 => 'product_cat','hide_empty' => 0,'title_li' 	 => '','hierarcial' => 1,'depth' => 0,] ); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>