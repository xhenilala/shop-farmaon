<?php
    /* Template Name: Blog*/
    get_header();
?>
<div class="container">
    <div class="row pt-3">
        <div class="col">
            <?php woocommerce_breadcrumb(); ?>
        </div>
    </div>
    <div class="row py-4">
        <div class="col">
            <?php view('general.page-title'); ?>
        </div>
    </div>
    <div class="row py-5">
        <?php  $query_args = [
            'post_type' => 'post',
            'posts_per_page' => 6,
            'paged' => get_query_var('paged'),
        ];
        $query = new WP_Query($query_args);
        if ($query->have_posts()) : ?>
            <?php while ($query->have_posts()) :
                $query->the_post(); ?>
                <div class="col-12 col-lg-6">
                    <?php view('general.article-card'); ?>
                </div>
            <?php endwhile; ?>
            <div>
        </div>
        <?php wp_reset_query(); ?>
        <?php endif; ?>
        
    </div>
</div>
<div class="container">
        <div class="row">
            <div class="col-12">
                <div class="farmaon-pagination text-center">
                    <?php do_action('wp_pagination', $query); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
