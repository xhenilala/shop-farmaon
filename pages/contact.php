<?php
    /* Template Name: Contact*/
    get_header();
?>
    <div class="default-page">
        <div class="container">
            <div class="row pt-3">
                <div class="col">
                    <?php woocommerce_breadcrumb(); ?>
                </div>
            </div>
            <div class="row pt-4">
                <div class="col">
                    <?php view('general.page-title'); ?>
                </div>
            </div>
            <div class="row py-5">
                <?php while(have_posts()): the_post(); ?>
                <div class="col">
                    <div class="default-page__content">
                        <?php the_content(); ?>
                    </div>
                    <div class="default-page__contact">
                        <?php echo do_shortcode('[vfb id=1]'); ?>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <?php view('general.location'); ?>
    </div>
<?php get_footer(); ?>