<?php
    /* Template Name: How to buy*/
    get_header();
?>
<div class="container pb-5">
    <div class="row pt-3">
        <div class="col">
            <?php woocommerce_breadcrumb(); ?>
        </div>
    </div>
    <div class="row py-4">
        <div class="col">
            <?php view('general.page-title'); ?>
        </div>
    </div>
    <?php $video = get_field('youtube_video'); ?>
    <?php if ($video) : ?>
        <div class="row py-4">
            <div class="col">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?php echo $video; ?>" allowfullscreen></iframe>
            </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="instructions py-5">
        <?php while (have_posts()) :
            the_post(); ?>
            <?php $instructions = collect(get_field('instructions'));
            $i = 1; ?>
            <?php foreach ($instructions as $instruction) : ?>
                <div class="instructions__item">
                    <div class="col-12 col-md-2 instructions__image">
                        <img src="<?php echo $instruction['image']; ?>" alt="how to buy" class="img-fluid">
                    </div>
                    <div class="col-12 col-md-10 instructions__description">
                        <p><?php echo $instruction['description']; ?></p>
                    </div>
                </div>
            <?php $i++;
            endforeach; ?>
        <?php endwhile; ?>
    </div>
</div>
<?php get_footer(); ?>
