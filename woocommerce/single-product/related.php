<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if (! defined('ABSPATH')) {
    exit;
}

if ($related_products) : ?>
    <div class="container py-5">
        <div class="row">
            <div class="col-12">
            <div class="related-products">
                <div class="related-products__title">
                    <h2><?php pll_e('Related products'); ?></h2>
                </div>

                <div class="related-products__container pt-3">
                    
					<?php woocommerce_product_loop_start(); ?>
						<?php foreach ($related_products as $related_product) : ?>
							<!-- <div class="cell"> -->
							<?php
								$post_object = get_post($related_product->get_id());

								setup_postdata($GLOBALS['post'] =& $post_object);

								wc_get_template_part('content-related', 'product'); ?>
								
							<!-- </div> -->
			
						<?php endforeach; ?>
					<?php woocommerce_product_loop_end(); ?>
                    
                </div>
                </div>
            </div>
        </div>
    </div>

<?php endif;

wp_reset_postdata();
