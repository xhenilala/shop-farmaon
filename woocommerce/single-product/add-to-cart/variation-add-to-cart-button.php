<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

global $product;
global $wpdb;
?>
 
<div class="woocommerce-variation-add-to-cart variations_button d-flex">
    <?php do_action('woocommerce_before_add_to_cart_button'); ?>
 
    <?php

    $variations = $product->get_children();
    if ($variations) {
        echo '<div class="product-color d-flex">';
        foreach ($variations as $variation) {
            $v = wc_get_product($variation);
            
            $variation_sku = $v->get_sku();
            $variation_attributes = $v->get_attributes(); 
            $color_codes = $wpdb->get_results("SELECT * FROM wp_color_codes ", ARRAY_A); 
            foreach ($color_codes as $color) {
                
                if ($variation_sku == $color['product_sku']) { 
                    $color_code = $color['color_code'];
                    echo '<div class="color-code mr-2" data-attribute="'.$variation_attributes['pa_color'].'" style="width:40px; height: 40px; background-color: #'.$color_code.'"></div>';
                }
            }
        }
        echo '</div>';
        echo '<br/>';
    }

?>

    <?php
    do_action('woocommerce_before_add_to_cart_quantity');

    woocommerce_quantity_input(array(
        'min_value'   => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
        'max_value'   => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
        'input_value' => isset($_POST['quantity']) ? wc_stock_amount(wp_unslash($_POST['quantity'])) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
    ));

    do_action('woocommerce_after_add_to_cart_quantity');
    ?>

    <button type="submit" class="single_add_to_cart_button btn btn-primary alt"><?php pll_e('Add to cart'); ?></button>

    <?php do_action('woocommerce_after_add_to_cart_button'); ?>

    <input type="hidden" name="add-to-cart" value="<?php echo absint($product->get_id()); ?>" />
    <input type="hidden" name="product_id" value="<?php echo absint($product->get_id()); ?>" />
    <input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
