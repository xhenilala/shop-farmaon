<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NMC_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.js"></script> -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> oncopy="return false" oncut="return false" onpaste="return false">

<div id="page" class="site d-flex flex-column">
	<?php view('parts.top-header'); ?> 
	<?php view('parts.header'); ?>
	<?php view('parts.mobile-header'); ?>
	<div id="content" class="site-content flex-grow-1 pt-0 pb-0">
