<?php

return [
    "prefix" => "nmc",

    "logs" => [
        /**
         * 
         * drivers: local, stackdriver
         * 
         */
        "driver" => "local",

        /**
         * 
         * name based on your project name
         * 
         */
        "logger_name" => "nmc",

        /**
         * 
         * only needed when you are using 'stackdriver' logs
         * 
         */
        "project_id" => "newmedia-logs",
        /**
         * 
         * json file in root of your theme directory
         * 
         */
        "credentials_file" => "wordpress-stackdriver",
    ],

    "supports" => [
        "title-tag" => null,
        "post-thumbnails" => null,
        "post-formats" => ['gallery', 'video'],
        "custom-header" => null,
        "custom-logo" => null,
        "html5" => [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ],
        "woocommerce" => null,
        "wc-product-gallery-lightbox" => null,
        "wc-product-gallery-slider" => null
    ],

    "admin_option_page" => [
        'page_title' => __('Options', 'farmaon'),
        'menu_title' => __('Options', 'farmaon'),
        'menu_slug' => 'options',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-admin-generic',
    ],

    'nav_menus' => [
        'primary' => __('Primary Menu', 'farmaon'),
        'after-main' => __('After Main Menu', 'farmaon'),
        'footer' => __('Footer Menu', 'farmaon'),
        'about-us' => __('About us', 'farmaon'),
        'service' => __('Client Service', 'farmaon'),
        'security' => __('Security', 'farmaon'),
    ],
    "sidebars" => [
        "sidebar" => [
            'name' => __('Sidebar', 'farmaon'),
            'description' => __('Default sidebar', 'farmaon'),
        ],
        "sidebar-search" => [
            'name' => __('Products Search', 'farmaon'),
            'description' => __('Products sidebar', 'farmaon'),
        ],
        "sidebar-filter" => [
            'name' => __('Products Filter', 'farmaon'),
            'description' => __('Products filter', 'farmaon'),
        ],
    ],

    "assets" => [
        "prefix" => "nmc",
        "build_dir" => "build",
        "stylesheet_files" => [
            '/styles/app.css',
        ],
        "global_scripts" => [
            '/scripts/jquery.js' => [
                'key' => 'jquery',
                'in_footer' => true,
            ],
        ],
        "scripts_files" => [
            '/scripts/app.js' => [
                'key' => 'app',
                'in_footer' => true,
                'deps' => ['jquery']
            ],
        ],
        "scripts_urls" => [
            'https://maps.googleapis.com/maps/api/js' => [
                'key' => 'google-maps',
                'params' => [
                    'v' => '3.exp',
                    'key' => 'AIzaSyB42ga4NVJW73TO7Jd05o7kyldh_eL1ko4',
                ],
                'in_footer' => true,
            ],
        ],
    ],
];
